#!/usr/bin/python
import sys
import math
try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser  # ver. < 3.0

def logistic(x):
    return 1.0/(1++math.exp(-x))

# instantiate
if len(sys.argv) < 2 :
        print("Usage: %s configfile" % sys.argv[0])
        exit(1)

configfile=sys.argv[1]
config = ConfigParser()
# parse existing file
config.read(configfile)
roi = 0
roi = config.getint('inputdata', 'round output to int')
nhl = config.getint('inputdata', 'number of hidden layers')
nni = config.getint('inputdata', 'number of neurons in input layer')
nno = config.getint('inputdata', 'number of neurons in the output layer')
biasinput = []
weightinput = []
shl="file with bias and weigths of input layer"
f=open(config.get('inputdata',shl),'rb')
for row in f:
    biasinput.append((row.split()).pop(0))
    weightinput.append(row.split()[1:])
#    print biasinput[-1], weightinput[-1]
f.close()
nnh = []
nnhlifn = []
for i in range(nhl):
        shl="number of neurons in hidden layer %d" % i
        nnh.append(config.getint('inputdata',shl))
        shl="file with bias and weigths of hidden layer %d" % i
        nnhlifn.append(config.get('inputdata',shl))
bias = []
weighthl = []
bias.append([])
bias.append([])
weighthl.append([])
weighthl.append([])
for i in range(nhl):
    f=open(nnhlifn[i],'rb')
    for row in f:
        bias[i].append((row.split()).pop(0))
        weighthl[i].append(row.split()[1:])
#        print bias[i], weighthl[i]
    f.close()
while True:
    input= []
    input = (raw_input("Enter input:\n")).split()
    sum = [0]*max(nnh)
    H = [0]*max(nnh)
    for j in range(nnh[0]):
        sum[j]=float(biasinput[j])
        for i in range(nni):
            sum[j]+=float(input[i])*float(weightinput[j][i])
    #        print j,i,sum[j]
        H[j]=logistic(sum[j])
    #    print H[j]
    for k in range(1,nhl):
        for j in range(nnh[k]):
            sum[j]=float(bias[k-1][j])
            for i in range(nnh[k-1]):
                sum[j]+=float(H[i])*float(weighthl[k-1][j][i])
            H[j]=logistic(sum[j])
    O = [0]*nno
    for j in range(nno):
        sum[j]=float(bias[nhl-1][j])
        for i in range(nnh[nhl-1]):
            sum[j]+=float(H[i])*float(weighthl[nhl-1][j][i])
        O[j]=logistic(sum[j])
        print "Output[",j,"]=",int(O[j]+0.5) if roi else O[j]
