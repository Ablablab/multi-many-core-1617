CHECKDIGITS=3900
sh "getdigits.sh" 2>/dev/null || true
RUNDIGITS=$(sdiff -s digits.nn digits.good | wc -l)
echo "Found " $RUNDIGITS ", expected " $CHECKDIGITS
if [ "$RUNDIGITS" -lt "$CHECKDIGITS" ]; then
   echo "IS CORRECT? Corretto! CORRECT_TRUE"
else
   echo "IS CORRECT? NO, NON CORRETTO CORRECT_FALSE: " $RUNDIGITS ", expected " $CHECKDIGITS
fi
