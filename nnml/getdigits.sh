#!/bin/bash
#if [ $# -lt 1 ]; then
#	echo "Usage: $0 output_file"
#	exit 1
#fi
sed -n 1,10p < train-results > hl_d2
sed -n 11,38p < train-results > hl_d1
sed -n 39,94p < train-results > il
cat test-input | python cnn.py cnn.ini | grep Output | cut -d ' ' -f 2,4 | python btd.py > digits.nn
