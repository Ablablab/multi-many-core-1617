#include <cuda_runtime.h>
#include <stdio.h>

#define CUDA_CHECK(call) {                                    \
    cudaError err = call;                                                    \
    if(cudaSuccess != err) {                                                \
        fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n",        \
                __FILE__, __LINE__, cudaGetErrorString( err) );              \
        exit(EXIT_FAILURE);                                                  \
    }}

#define THREADSPERBLOCK 512
#define THREADSPERWARP 32

#define memcpyArrayToGPU(gpu_pointer,cpu_pointer,size)   \
    CUDA_CHECK(cudaMalloc((void **)&gpu_pointer, size));   \
    CUDA_CHECK(cudaMemcpy(gpu_pointer, cpu_pointer, size, cudaMemcpyHostToDevice)); \
  

#define memcpyArrayToCPU(gpu_pointer, cpu_pointer, size)                           \
    CUDA_CHECK(cudaMemcpy(cpu_pointer, gpu_pointer, size, cudaMemcpyDeviceToHost))
