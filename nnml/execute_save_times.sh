COMMAND=$1
PARAMETERS=$2
#CHECKCORRECT="./check_correct.sh"
CHECKCORRECT="./check_same_results.sh"
OUTPUT=$COMMAND-$PARAMETERS.log
NOTE=$3
echo "--------------------------------------------------------------------------------------------------------">>$OUTPUT 2>&1
echo "RUN OF " $(date) >>$OUTPUT 2>&1
echo "note: " $NOTE >>$OUTPUT 2>&1
perf stat -e instructions -e cycles -e cache-references -e cache-misses $COMMAND -i $PARAMETERS >>$OUTPUT 2>&1
ISCORRECT=$($CHECKCORRECT)
echo $ISCORRECT >>$OUTPUT 2>&1
echo $ISCORRECT
echo "--------------------------------------------------------------------------------------------------------">>$OUTPUT 2>&1
echo " ">>$OUTPUT 2>&1
echo " ">>$OUTPUT 2>&1
