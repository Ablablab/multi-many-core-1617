CHECKDIGITS=1
#sh "getdigits.sh" 2>/dev/null || true
RUNDIGITS=$(diff train-results train-results_updated | wc -l)
echo "Found " $RUNDIGITS " differences with old results"
if [ "$RUNDIGITS" -lt "$CHECKDIGITS" ]; then
   echo "IS CORRECT? Corretto! CORRECT_TRUE"
else
   echo "IS CORRECT? NO, NON CORRETTO CORRECT_FALSE: " $RUNDIGITS ", expected " $CHECKDIGITS
fi
