/*******************************************************************************
*   original code by                                                           *
*   JOHN BULLINARIA  2004. Modified by Massimo Bernaschi 2016                  *
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <fcntl.h>
#include <sys/time.h>

#include "common.h"
#include "dictionary.h"
#include "iniparser.h"

#define REAL float
#define NULLFILE "/dev/null"
#define DEFMAXEPOCH 1000
#if !defined(MAX)
#define MAX(a,b) ((a)>(b)?(a):(b))
#endif

// timers
#define TIMER_DEF struct timeval temp_1, temp_2
#define TIMER_START gettimeofday(&temp_1, (struct timezone*)0)
#define TIMER_STOP gettimeofday(&temp_2, (struct timezone*)0)
#define TIMER_ELAPSED_SEC (temp_2.tv_sec-temp_1.tv_sec)
#define TIMER_ELAPSED_USEC (temp_2.tv_usec-temp_1 .tv_usec)
#define LAP(lap) TIMER_STOP;fprintf(stderr,"point %f:  %ld s + %d\n" ,(lap), TIMER_ELAPSED_SEC, TIMER_ELAPSED_USEC);TIMER_START

void Usage(char *cmd) {
  printf("-----------------------\n");
  printf("Neural Networks Learning Code (by backpropagation)\n");
  printf("-----------------------\n");
  printf("Usage: %s \n"
  "-i inputfile  \n"
  "[-v verbose \n -D Debug \n  -h ThisHelp]\n",
  cmd);
}

void ReadFromFile(char *fn,float **array,int nrow, int ncol, int ts) {
  FILE *fp=NULL;
  int i, j;
  double **dp;
  float  **sp;
  switch(ts) {
    case sizeof(float):
    sp=(float **)array;
    break;
    case sizeof(double):
    dp=(double **)array;
    break;
    default:
    writelog(TRUE,APPLICATION_RC,"invalid size in ReadFromFile: %d\n",ts);
    break;
  }
  fp=Fopen(fn,"r");
  for(i=0; i<nrow; i++) {
    for(j=0; j<ncol; j++) {
      switch(ts) {
        case sizeof(float):
        fscanf(fp,"%f",&(sp[i][j]));
        break;
        case sizeof(double):
        fscanf(fp,"%lf",&(dp[i][j]));
        break;
        default:
        writelog(TRUE,APPLICATION_RC,"invalid size in ReadFromFile: %d\n",ts);
        break;
      }
    }
  }
  fclose(fp);
}


int main(int argc, char *argv[]) {
  TIMER_DEF;
  TIMER_START;
  fprintf(stderr, "START\n");
  int     h, i, j, k, p, np, op, epoch;
  int    NumPattern, NumInput, NumHidden, NumOutput;
  /*
  double Input[NUMPAT+1][NUMIN+1] = { 0, 0, 0,  0, 0, 0,  0, 1, 0,  0, 0, 1,  0, 1, 1 };
  double Target[NUMPAT+1][NUMOUT+1] = { 0, 0,  0, 0,  0, 1,  0, 1,  0, 0 };
  double SumH[NUMPAT+1][NUMHID+1], WeightIH[NUMIN+1][NUMHID+1], Hidden[NUMPAT+1][NUMHID+1];
  double SumO[NUMPAT+1][NUMOUT+1], WeightHO[NUMHID+1][NUMOUT+1], Output[NUMPAT+1][NUMOUT+1];
  double DeltaO[NUMPAT+1][NUMOUT+1], SumDOW[NUMHID+1], DeltaH[NUMPAT+1][NUMHID+1];
  double DeltaWeightIH[NUMIN+1][NUMHID+1], DeltaWeightHO[NUMHID+1][NUMOUT+1];
  double Error, eta = 0.5, alpha = 0.9, smallwt = 0.5;
  */
  REAL **Input;
  REAL **Target;
  REAL **Sum;
  REAL **WeightIH, **Hidden;
  REAL ***H2H, ***DeltaH2H;
  REAL ***WeightH2H, ***DeltaWeightH2H;
  REAL **Htemp, **Deltatemp;
  REAL **WeightHO, **Output;
  REAL **DeltaO, *SumDOW, **DeltaH;
  REAL **DeltaWeightIH, **DeltaWeightHO;
  REAL Error, Eps, eta, alpha, smallwt;
  int *ranpat;
  int verbose=FALSE;
  int maxepoch=DEFMAXEPOCH;
  int dimsum=0;
  int NumHL=1;
  int *nupl=NULL;
  char *inputfile = NULL;
  char *po;
  dictionary *ini;
  char key[MAXSTRLEN];
  char formatstring[MAXSTRLEN];
  char LogFileName[MAXSTRLEN];
  char InputFileName[MAXSTRLEN];
  char TargetFileName[MAXSTRLEN];
  char ResultFileName[MAXSTRLEN];
  char RestartFileName[MAXSTRLEN];
  char DeltaFileName[MAXSTRLEN];
  char RestartDeltaFileName[MAXSTRLEN];
  FILE *fp=NULL;
  FILE *fpd=NULL;
  FILE *fpl=NULL;
  if(sizeof(REAL)==sizeof(float)) {
    strcpy(formatstring,"%f ");
  }
  if(sizeof(REAL)==sizeof(double)) {
    strcpy(formatstring,"%lf ");
  }

  for(i = 1; i < argc; i++) {
    po = argv[i];
    if (*po++ == '-') {
      switch (*po++) {
        case 'h':
        Usage(argv[0]);
        exit(OK);
        break;
        case 'v':
        verbose=TRUE;
        break;
        case 'i':
        SKIPBLANK
        inputfile=Strdup(po);
        break;
        default:
        Usage(argv[0]);
        exit(OK);
        break;
      }
    }
  }
  if(inputfile==NULL) {
    Usage(argv[0]);
    exit(OK);
  }

  ini = iniparser_load(inputfile);

  if(ini==NULL) { writelog(TRUE,APPLICATION_RC,"Cannot parse file: %s\n", inputfile); }

  READINTFI(maxepoch,"Max number of epochs");
  READINTFI(NumPattern,"Number of training data");
  READINTFI(NumInput,"Number of input units");
  READINTFI(NumOutput,"Number of output units");
  READINTFI(NumHL,"Number of hidden layers");
  READREALFI(eta,"Learning rate");
  READREALFI(alpha,"Momentum");
  READREALFI(smallwt,"Initialization scale");
  READREALFI(Eps,"Error threshold");
  {READSTRFI(LogFileName,"Log file name");}
  {READSTRFI(InputFileName,"Input file name");}
  {READSTRFI(TargetFileName,"Target file name");}
  {READSTRFI(ResultFileName,"Results file name");}
  {READSTRFI(DeltaFileName,"Result delta file name");}
  {READSTRFI(RestartFileName,"Restart file name");}
  {READSTRFI(RestartDeltaFileName,"Restart delta file name");}
  nupl=makevect(NumHL+2,sizeof(int));
  nupl[0]=NumInput;
  nupl[NumHL+1]=NumOutput;
  if(NumHL) {
    int scratch;
    char tempstring[MAXSTRLEN];
    H2H=(REAL ***)Malloc(sizeof(REAL *)*(NumHL));
    WeightH2H=(REAL ***)Malloc(sizeof(REAL *)*(NumHL));
    DeltaH2H=(REAL ***)Malloc(sizeof(REAL *)*(NumHL));
    DeltaWeightH2H=(REAL ***)Malloc(sizeof(REAL *)*(NumHL));
    for(i=1; i<=NumHL; i++) {
      snprintf(tempstring,sizeof(tempstring),"Number of units in layer %d",i-1);
      READINTFI(scratch,tempstring);
      nupl[i]=scratch;
      H2H[i-1]=(REAL **)makematr(NumPattern, nupl[i]+1,sizeof(REAL));
      DeltaH2H[i-1]=(REAL **)makematr(NumPattern, nupl[i]+1,sizeof(REAL));
    }
    for(i=1; i<=NumHL; i++) {
      DeltaWeightH2H[i-1]=(REAL **)makematr(nupl[i-1]+1,nupl[i]+1,sizeof(REAL));
      WeightH2H[i-1]=(REAL **)makematr(nupl[i-1]+1,nupl[i]+1,sizeof(REAL));
    }
  }

  dimsum=nupl[1];
  for(i=1; i<=(NumHL+1); i++) {
    dimsum=MAX(dimsum,nupl[i]);
  }
  Input=(REAL **)makematr(NumPattern, NumInput,sizeof(REAL));
  Target=(REAL **)makematr(NumPattern, NumOutput,sizeof(REAL));
  Sum=(REAL **)makematr(NumPattern, dimsum+1,sizeof(REAL));
  Output=(REAL **)makematr(NumPattern, NumOutput+1,sizeof(REAL));
  DeltaO=(REAL **)makematr(NumPattern, NumOutput+1,sizeof(REAL));
  WeightHO=(REAL **)makematr(nupl[NumHL]+1, NumOutput+1,sizeof(REAL));
  DeltaWeightHO=(REAL **)makematr(nupl[NumHL]+1, NumOutput+1,sizeof(REAL));
  SumDOW=makevect(dimsum+1,sizeof(REAL));
  ranpat=makevect(NumPattern,sizeof(int));

  ReadFromFile(InputFileName,Input,NumPattern,NumInput,sizeof(REAL));
  ReadFromFile(TargetFileName,Target,NumPattern,NumOutput,sizeof(REAL));

  LAP(0.);

  if(strcmp(LogFileName,NULLFILE)) { fpl=Fopen(LogFileName,"w"); }

  for( k = 0 ; k <= nupl[NumHL+1] ; k ++ ) {    /* initialize WeightHO and DeltaWeightHO */
    for( j = 0 ; j <= nupl[NumHL] ; j++ ) {
      DeltaWeightHO[j][k] = 0.0 ;
      WeightHO[j][k] = 2.0 * ( drand48() - 0.5 ) * smallwt ;
    }
  }
  for(h=NumHL; h>0; h--) {
    for(j=0; j<=nupl[h]; j++) {
      for(i=0; i<=nupl[h-1]; i++) {
        DeltaWeightH2H[h-1][i][j] = 0.0 ;
        WeightH2H[h-1][i][j] = 2.0 * ( drand48() - 0.5 ) * smallwt ;
      }
    }
  }
  if(strcmp(RestartFileName,NULLFILE)) {
    fp=Fopen(RestartFileName,"r");
    if(strcmp(RestartDeltaFileName,NULLFILE)) {
      fpd=Fopen(RestartDeltaFileName,"r");
    }
    for( k = 1 ; k <= nupl[NumHL+1] ; k ++ ) {
      fscanf(fp,formatstring,&WeightHO[0][k]);
      if(fpd) fscanf(fpd,formatstring,&DeltaWeightHO[0][k]);
      for( j = 1 ; j <= nupl[NumHL] ; j++ ) {
        fscanf(fp,formatstring,&WeightHO[j][k]);
        if(fpd) fscanf(fpd,formatstring,&DeltaWeightHO[j][k]);
      }
      fscanf(fp,"\n");
      if(fpd) fscanf(fpd,"\n");
    }

    for(h=NumHL; h>0; h--) {
      for( j = 1 ; j <= nupl[h] ; j++ ) {
        fscanf(fp,formatstring,&WeightH2H[h-1][0][j]);
        if(fpd) fscanf(fpd,formatstring,&DeltaWeightH2H[h-1][0][j]);
        for( i = 1 ; i <= nupl[h-1] ; i++ ) {
          fscanf(fp,formatstring,&WeightH2H[h-1][i][j]);
          if(fpd) fscanf(fpd,formatstring,&DeltaWeightH2H[h-1][i][j]);
        }
        fscanf(fp,"\n");
        if(fpd)fscanf(fpd,"\n");
      }
    }

    if (fp) fclose(fp);
    if (fpd) fclose(fpd);
  }

  LAP(1.);

  if(verbose) {
    printf("\nInitial Bias and Weights\n");
    for( k = 1 ; k <=  nupl[NumHL+1] ; k ++ ) {
      printf("Bias H to O[%d]: %f\n",k,WeightHO[0][k]);
      for( j = 1 ; j <= nupl[NumHL] ; j++ ) {
        printf("Weight H[%d] to O[%d]: %f\n",j,k,WeightHO[j][k]);
      }
    }
    for(h=NumHL; h>0; h--) {
      for( j = 1 ; j <= nupl[h] ; j++ ) {
        printf("Bias[%d][%d]: %f\n",h-1,j,WeightH2H[h-1][0][j]);
        for( i = 1 ; i <= nupl[h-1] ; i++ ) {
          printf("Weight[%d][%d][%d]: %f\n",h-1,i,j,WeightH2H[h-1][i][j]);
        }
      }
    }
  }
  for( p = 0 ; p < NumPattern ; p++ ) {    /* initialize order of individuals */
    ranpat[p] = p ;
  }

  LAP(2.);

  for( epoch = 0 ; epoch < maxepoch ; epoch++) {    /* iterate weight updates */
    #if defined(RANDOMIZE_INDIVIDUALS)
    for( p = 0 ; p <  NumPattern ; p++ ) {    /* randomize order of individuals */
      ranpat[p] = p ;
    }
    for( p = 0 ; p < NumPattern-1 ; p++) {
      np = (p+1) + (rand()%(NumPattern - p -1)) ;
      op = ranpat[p] ; ranpat[p] = ranpat[np] ; ranpat[np] = op ;
    }
    #endif

    Error = 0.0 ;
    for( np = 0 ; np < NumPattern ; np++ ) {
      p = ranpat[np];
      for( j = 1 ; j <= nupl[1]; j++ ) {    /* compute hidden unit activations */
        Sum[p][j] = WeightH2H[0][0][j] ;
        for( i = 1 ; i <= nupl[0] ; i++ ) {
          Sum[p][j] += Input[p][i-1] * WeightH2H[0][i][j] ; /* Matrix SumH = Input_Matrix x Weight_Input_Matrix
          The Input_Matrix has one row per sample
          The Weight_Input_Matrix has one row per input
          The SumH Matrix is initialized with the Bias */
        }
        H2H[0][p][j] = 1.0/(1.0 + exp(-Sum[p][j])) ;    /* Compute the sigmoid of all the elements of SumH */
      }
      for( h=1; h<NumHL; h++) {
        for(k=1; k<=nupl[h+1]; k++) {
          Sum[p][k] = WeightH2H[h][0][k];
          for( j = 1 ; j <= nupl[h] ; j++ ) {
            Sum[p][k] += H2H[h-1][p][j] * WeightH2H[h][j][k] ;/* Matrix SumO = Hidden_Matrix x Weight_Output Matrix
            The Hidden_Matrix has one row per sample
            The Weight_Output_Matrix has one row per number of neurons in the hidden level
            The SumO Matrix is initialized with the Bias */
          }
          H2H[h][p][k] = 1.0/(1.0 + exp(-Sum[p][k])) ;   /* Sigmoidal Outputs *//* Compute the sigmoid of all the elements of SumO */
        }
      }
      for( k = 1 ; k <= nupl[NumHL+1] ; k++ ) {    /* compute output unit activations and errors */
        Sum[p][k] = WeightHO[0][k] ;
        for( j = 1 ; j <= nupl[NumHL] ; j++ ) {
          Sum[p][k] += H2H[NumHL-1][p][j] * WeightHO[j][k] ;/* Matrix SumO = Hidden_Matrix x Weight_Output Matrix
          The Hidden_Matrix has one row per sample
          The Weight_Output_Matrix has one row per number of neurons in the hidden level
          The SumO Matrix is initialized with the Bias */
        }
        Output[p][k] = 1.0/(1.0 + exp(-Sum[p][k])) ;   /* Sigmoidal Outputs *//* Compute the sigmoid of all the elements of SumO */
        /*              printf("Epoch %d, pattern %d, output %d, output %f, target %f\n", epoch, p, k, Output[p][k], Target[p][k-1]); */
        /*              Output[p][k] = SumO[p][k];      Linear Outputs */

        Error += 0.5 * (Target[p][k-1] - Output[p][k]) * (Target[p][k-1] - Output[p][k]) ;   /* SSE */
        /*              Error -= ( Target[p][k-1] * log( Output[p][k] ) + ( 1.0 - Target[p][k-1] ) * log( 1.0 - Output[p][k] ) ) ;    Cross-Entropy Error */
        DeltaO[p][k] = (Target[p][k-1] - Output[p][k]) * Output[p][k] * (1.0 - Output[p][k]) ;   /* Sigmoidal Outputs, SSE */
        /* derivative of the error x derivative of the sigmoidal function */
        /*              DeltaO[p][k] = Target[p][k-1] - Output[p][k];     Sigmoidal Outputs, Cross-Entropy Error */
        /*              DeltaO[p][k] = Target[p][k-1] - Output[p][k];     Linear Outputs, SSE */
      }
      for( j = 1 ; j <= nupl[NumHL] ; j++ ) {    /* 'back-propagate' errors to hidden layer */
      SumDOW[j] = 0.0 ;
      for( k = 1 ; k <= nupl[NumHL+1] ; k++ ) {
        SumDOW[j] += WeightHO[j][k] * DeltaO[p][k];
      }
      DeltaH2H[NumHL-1][p][j] = SumDOW[j] * H2H[NumHL-1][p][j] * (1.0 - H2H[NumHL-1][p][j]) ;
    }
    for(h=NumHL-1; h>0; h--) {
      for( j = 1 ; j <= nupl[h] ; j++ ) {
        SumDOW[j] = 0.0 ;
        for( k = 1 ; k <= nupl[h+1] ; k++ ) {
          SumDOW[j] += WeightH2H[h-1][j][k] * DeltaH2H[h][p][k];
        }
        DeltaH2H[h-1][p][j] = SumDOW[j] * H2H[h-1][p][j] * (1.0 - H2H[h-1][p][j]) ;
      }
    }
  }



  LAP(3.);



  for( k = 1 ; k <= nupl[NumHL+1] ; k ++ ) {
    DeltaWeightHO[0][k] = alpha * DeltaWeightHO[0][k];
    for( np = 0 ; np < NumPattern ; np++ ) {
      p = ranpat[np];
      DeltaWeightHO[0][k] += eta * DeltaO[p][k];
    }
  }

  LAP(3.1);

  for( k = 1 ; k <= nupl[NumHL+1] ; k ++ ) {
    for( j = 1 ; j <= nupl[NumHL] ; j++ ) {
      DeltaWeightHO[j][k] = alpha * DeltaWeightHO[j][k];
      for( np = 0 ; np < NumPattern ; np++ ) {
        p = ranpat[np];
        DeltaWeightHO[j][k] += eta * H2H[NumHL-1][p][j] * DeltaO[p][k]; /* Matrix DeltaWeightHO = eta x (Trasposte of Hidden_Matrix) x DeltaO */
        /* DeltaWeight is initialized with the "momentum" */
      }
    }
  }

  LAP(3.2);

  for(h=NumHL; h>0; h--) {
    for( j = 1 ; j <= nupl[h] ; j++ ) {
      DeltaWeightH2H[h-1][0][j] = alpha * DeltaWeightH2H[h-1][0][j];
      for( np = 0 ; np < NumPattern ; np++ ) {
        p = ranpat[np];
        DeltaWeightH2H[h-1][0][j] += eta * DeltaH2H[h-1][p][j];
      }
    }
    for( j = 1 ; j <= nupl[h] ; j++ ) {
      for( i = 1 ; i <= nupl[h-1] ; i++ ) {
        DeltaWeightH2H[h-1][i][j] = alpha * DeltaWeightH2H[h-1][i][j];
        for( np = 0 ; np < NumPattern ; np++ ) {
          p = ranpat[np];
          DeltaWeightH2H[h-1][i][j] += eta * ((h>1)?H2H[h-2][p][i]:Input[p][i-1]) * DeltaH2H[h-1][p][j];  /* Matrix DeltaWeightIH = eta x (Trasposte of Input_Matrix) x DeltaH */
          /* DeltaWeight is initialized with the "momentum" */
        }
      }
    }
  }

  LAP(4.);

  for( k = 1 ; k <= NumOutput ; k++ ) {    /* update weights WeightHO */
    WeightHO[0][k] += DeltaWeightHO[0][k] ;
    for( j = 1 ; j <= nupl[NumHL] ; j++ ) {
      WeightHO[j][k] += DeltaWeightHO[j][k] ;
    }
  }
  for(h=NumHL; h>0; h--) {
    for( j = 1 ; j <= nupl[h] ; j++ ) {
      WeightH2H[h-1][0][j] += DeltaWeightH2H[h-1][0][j];
      for( i = 1 ; i <= nupl[h-1] ; i++ ) {
        WeightH2H[h-1][i][j] += DeltaWeightH2H[h-1][i][j];
      }
    }
  }

  LAP(5.);

  fprintf(stdout, "Epoch %-5d :   Error = %f\n", epoch, Error) ;
  if(fpl) { fprintf(fpl,"Epoch %-5d :   Error = %f\n", epoch, Error);
  fflush(fpl); }

  if( Error < Eps ) break ;  /* stop learning when 'near enough' */
}

#if 0
fprintf(stdout, "\n\nNETWORK DATA - EPOCH %d\n\nPat\t", epoch) ;   /* print network outputs */
for( i = 0 ; i < NumInput ; i++ ) {
fprintf(stdout, "Input%-4d\t", i) ;
}
for( k = 0 ; k < NumOutput ; k++ ) {
fprintf(stdout, "Target%-4d\tOutput%-4d\t", k, k) ;
}
for( p = 0 ; p < NumPattern ; p++ ) {
fprintf(stdout, "\n%d\t", p) ;
for( i = 0 ; i < NumInput ; i++ ) {
fprintf(stdout, "%f\t", Input[p][i]) ;
}
for( k = 1 ; k <= NumOutput ; k++ ) {
fprintf(stdout, "%f\t%f\t", Target[p][k-1], Output[p][k]) ;
}
}
#endif
if(verbose) {
  printf("\nFinal Bias and Weights\n");
}
fp=Fopen(ResultFileName,"w");
fpd=Fopen(DeltaFileName,"w");
for( k = 1 ; k <= NumOutput ; k ++ ) {
  if(verbose) {
    printf("Bias H to O[%d]: %f\n",k,WeightHO[0][k]);
  }
  fprintf(fp,"%7.5f ",WeightHO[0][k]);
  fprintf(fpd,"%g ",DeltaWeightHO[0][k]);
  for( j = 1 ; j <= nupl[NumHL] ; j++ ) {
    if(verbose) {
      printf("Weight H[%d] to O[%d]: %f\n",j,k,WeightHO[j][k]);
    }
    fprintf(fp,"%7.5f ",WeightHO[j][k]);
    fprintf(fpd,"%g ",DeltaWeightHO[j][k]);
  }
  fprintf(fp,"\n");
  fprintf(fpd,"\n");
}
for(h=NumHL; h>0; h--) {
  for( j = 1 ; j <= nupl[h] ; j++ ) {
    if(verbose) {
      printf("BiasH2H[%d][%d]: %f\n",h,j,WeightH2H[h-1][0][j]);
    }
    fprintf(fp,"%7.5f ",WeightH2H[h-1][0][j]);
    fprintf(fpd,"%g ",DeltaWeightH2H[h-1][0][j]);
    for( i = 1 ; i <= nupl[h-1] ; i++ ) {
      if(verbose) {
        printf("WeightH2H[%d][%d] to H{%d]: %f\n",h,i,j,WeightH2H[h-1][i][j]);
      }
      fprintf(fp,"%7.5f ",WeightH2H[h-1][i][j]);
      fprintf(fpd,"%g ",DeltaWeightH2H[h-1][i][j]);
    }
    fprintf(fp,"\n");
    fprintf(fpd,"\n");
  }
}

LAP(6);

if(fp) fclose(fp);
if(fp) fclose(fpd);
if(fpl) fclose(fpl);
return 0 ;
}

/*******************************************************************************/
