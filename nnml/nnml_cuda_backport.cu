/*******************************************************************************
*   original code by                                                           *
*   JOHN BULLINARIA  2004. Modified by Massimo Bernaschi 2016                  *
*      Parallelized and Cudaconverted by Andrea Gennusa 2017                   *
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <fcntl.h>
#include <sys/time.h>
#include <cuda.h>
#include <cuda_runtime.h>

#include "common.h"
#include "dictionary.h"
#include "iniparser.h"

#define REAL float
#define NULLFILE "/dev/null"
#define DEFMAXEPOCH 1000
#define REALBYTES sizeof(REAL)
#if !defined(MAX)
#define MAX(a,b) ((a)>(b)?(a):(b))
#endif


// timers
#define TIMER_DEF struct timeval temp_1, temp_2
#define TIMER_START gettimeofday(&temp_1, (struct timezone*)0)
#define TIMER_STOP gettimeofday(&temp_2, (struct timezone*)0)
#define TIMER_ELAPSED_SEC (temp_2.tv_sec-temp_1.tv_sec)
#define TIMER_ELAPSED_USEC (temp_2.tv_usec-temp_1 .tv_usec)
#define LAP(lap) TIMER_STOP;fprintf(stderr,"point %f:  %lf s \n" ,(lap), TIMER_ELAPSED_SEC*1. + TIMER_ELAPSED_USEC/1000000.);TIMER_START
#define TAKETIME 1
#define MAX_SIZE_VECTOR 4
cudaError_t err;

#define CUDA_CHECK(call) {                                    \
  cudaError err = call;                                                    \
  if(cudaSuccess != err) {                                                \
    fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n",        \
    __FILE__, __LINE__, cudaGetErrorString( err) );              \
    exit(EXIT_FAILURE);                                                  \
  }}

  #define CUDA_CHECK_KERNEL { \
    err = cudaGetLastError(); \
    if (err != cudaSuccess)  \
    fprintf(stderr,"Error: %s\n", cudaGetErrorString(err)); \
  }

  #define THREADSPERBLOCK 256
  #define THREADSPERWARP 32
  #define TILE_WIDTH 16


  #define VECTOR_TYPE REAL
  // this architecture feature is discussed in relation
  struct Vector_2D {
    VECTOR_TYPE * vector;
    int rows;
    int cols;
    //size_t size_unit;
  };

  // this architecture feature is discussed in relation
  struct Vector_3D {
    VECTOR_TYPE * vector;
    int x_dim;
    //standard c99 solves this problem, you can declare struct with int vec[] without size .
    int y_dim[MAX_SIZE_VECTOR];
    int z_dim[MAX_SIZE_VECTOR];
    //size_t size_unit;
  };


  struct Vector_2D malloc_2D(int rows, int columns){
    VECTOR_TYPE * pointer =(VECTOR_TYPE *) malloc(rows * columns * sizeof(VECTOR_TYPE));
    if (pointer == NULL){
      fprintf(stderr, "Error in MALLOC %d x %d of size unit %d\n", rows, columns, sizeof(VECTOR_TYPE));
      exit(EXIT_FAILURE);
    }
    struct Vector_2D vector;
    vector.vector = pointer;
    vector.rows = rows;
    vector.cols = columns;

    return vector;
  }

  static inline int index_2D(struct Vector_2D matrix, int x, int y){
    return x*(matrix.cols)+y;
  }

  __device__ static inline int index_2D_gpu(struct Vector_2D matrix, int x, int y){
    return x*(matrix.cols)+y;
  }

  static inline int index_3D(struct Vector_3D hypmatrix, int x, int y, int z){
    int x_t = x;
    int index = y * hypmatrix.z_dim[x_t] + z;
    for (;x_t>0;x_t--){
      index += hypmatrix.z_dim[x_t-1] * hypmatrix.y_dim[x_t-1];
    }
    return index;
  }

  __device__ static inline int index_3D_gpu(struct Vector_3D hypmatrix, int x, int y, int z){
    int x_t = x;
    int index = y * hypmatrix.z_dim[x] + z;
    for (;x_t>0;x_t--){
      index += hypmatrix.z_dim[x_t-1] * hypmatrix.y_dim[x_t-1];
    }
    return index;
  }

  struct Vector_3D malloc_3D(int x_dim, int y_dim[], int z_dim[]){
    struct Vector_3D vector;
    vector.x_dim = x_dim;

    int size = 0;
    int i, y_temp, z_temp;
    for (i = 0; i<x_dim;i++){
      y_temp = y_dim[i];
      z_temp = z_dim[i];
      vector.y_dim[i] = y_temp;
      vector.z_dim[i] = z_temp;
      size += y_temp * z_temp;
    }
    VECTOR_TYPE * pointer =(VECTOR_TYPE *) malloc(size * sizeof(VECTOR_TYPE));
    if (pointer == NULL){
      fprintf(stderr, "Error in MALLOC 3D %d size unit %d\n", x_dim, sizeof(VECTOR_TYPE));
      exit(EXIT_FAILURE);
    }

    vector.vector = pointer;
    return vector;
  }

  void * copy1DToGPU(void * vector, size_t size){
    void * vector_gpu;
    CUDA_CHECK(cudaMalloc((void **)&vector_gpu, size));
    CUDA_CHECK(cudaMemcpy(vector_gpu, vector, size, cudaMemcpyHostToDevice));
    return vector_gpu;
  }

  void * copy1DToCPU(void * vector_gpu, void * vector_cpu, size_t size){
    CUDA_CHECK(cudaMemcpy(vector_cpu, vector_gpu, size, cudaMemcpyDeviceToHost));
    return vector_cpu;
  }


  struct Vector_2D copy2DToGPU(struct Vector_2D vector_cpu){
    struct Vector_2D vector_gpu;
    vector_gpu.rows = vector_cpu.rows;
    vector_gpu.cols = vector_cpu.cols;
    size_t size = vector_gpu.rows*vector_gpu.cols*sizeof(VECTOR_TYPE);
    CUDA_CHECK(cudaMalloc((void **)&vector_gpu.vector, size));
    CUDA_CHECK(cudaMemcpy(vector_gpu.vector, vector_cpu.vector, size, cudaMemcpyHostToDevice));
    return vector_gpu;
  }

  struct Vector_3D copy3DToGPU(struct Vector_3D vector_cpu){
    struct Vector_3D vector_gpu;
    vector_gpu.x_dim = vector_cpu.x_dim;
    int i = 0;
    size_t size = 0;
    for(i=0; i<vector_gpu.x_dim; i++){
      vector_gpu.y_dim[i] = vector_cpu.y_dim[i];
      vector_gpu.z_dim[i] = vector_cpu.z_dim[i];
      size += vector_gpu.z_dim[i] * vector_gpu.y_dim[i];
    }

    size *= sizeof(VECTOR_TYPE);
    CUDA_CHECK(cudaMalloc((void **)&vector_gpu.vector, size));
    CUDA_CHECK(cudaMemcpy(vector_gpu.vector, vector_cpu.vector, size, cudaMemcpyHostToDevice));
    return vector_gpu;
  }

  struct Vector_2D copy2DToCPU(struct Vector_2D vector_gpu, struct Vector_2D vector_cpu){
    size_t size = vector_cpu.rows*vector_cpu.cols*sizeof(VECTOR_TYPE);

    CUDA_CHECK(cudaMemcpy(vector_cpu.vector, vector_gpu.vector, size, cudaMemcpyDeviceToHost));
    return vector_cpu;
  }

  struct Vector_3D copy3DToCPU(struct Vector_3D vector_gpu, struct Vector_3D vector_cpu){
    int i = 0;
    size_t size = 0;
    for(i=0; i<vector_cpu.x_dim; i++){
      size += vector_cpu.z_dim[i] * vector_cpu.y_dim[i];
    }

    size *= sizeof(VECTOR_TYPE);
    CUDA_CHECK(cudaMemcpy(vector_cpu.vector, vector_gpu.vector, size, cudaMemcpyDeviceToHost));
    return vector_cpu;
  }




  void Usage(char *cmd) {
    printf("-----------------------\n");
    printf("Neural Networks Learning Code (by backpropagation)\n");
    printf("-----------------------\n");
    printf("Usage: %s \n"
    "-i inputfile  \n"
    "[-v verbose \n -D Debug \n  -h ThisHelp]\n",
    cmd);
  }

  void ReadFromFile(char *fn,struct Vector_2D array,int nrow, int ncol, int ts) {
    FILE *fp=NULL;
    int i, j;
    double *dp;
    float  *sp;
    switch(ts) {
      case sizeof(float):
      sp=(float *)array.vector;
      break;
      case sizeof(double):
      dp=(double *)array.vector;
      break;
      default:
      writelog(TRUE,APPLICATION_RC,"invalid size in ReadFromFile: %d\n",ts);
      break;
    }
    fp=Fopen(fn,"r");
    for(i=0; i<nrow; i++) {
      for(j=0; j<ncol; j++) {
        switch(ts) {
          case sizeof(float):
          fscanf(fp,"%f",&(sp[index_2D(array, i,j)]));
          break;
          case sizeof(double):
          fscanf(fp,"%lf",&(dp[index_2D(array, i,j)]));
          break;
          default:
          writelog(TRUE,APPLICATION_RC,"invalid size in ReadFromFile: %d\n",ts);
          break;
        }
      }
    }
    fclose(fp);
  }












  /*

  CUDA

  */
  __constant__ int NumPattern_gpu[1];
  __constant__ int NumHL_gpu[1];
  __constant__ int NumOutput_gpu[1];


  __global__ void kernel0(int * ranpat, struct Vector_2D Sum, struct Vector_3D WeightH2H, struct Vector_2D Input, struct Vector_3D H2H, int * nupl, int h){
    int nupl0 = nupl[0];
    int nupl1 = nupl[1];
    int bx = blockIdx.x, by = blockIdx.y,
    tx = threadIdx.x, ty = threadIdx.y,
    i = by * TILE_WIDTH + ty,
    j = bx * TILE_WIDTH + tx +1;




    __shared__ REAL A[TILE_WIDTH][TILE_WIDTH];
    __shared__ REAL B[TILE_WIDTH][TILE_WIDTH];
    int rA = *NumPattern_gpu;
    int cA = nupl0;
    int rB = nupl0+1;
    int cB = nupl1+1;
    int rC = *NumPattern_gpu;
    int cC = nupl1+1;


    REAL SumThread = 0;


    for (int m = 0; m < (cA-1)/TILE_WIDTH+1; ++m) {
      if (i<rA && m*TILE_WIDTH+tx < cA) {
        A[ty][tx] = Input.vector[index_2D_gpu(Input,i,m*TILE_WIDTH+tx)];
      } else {
        A[ty][tx] = 0;
      }

      if (j<cB && m*TILE_WIDTH+ty+1 < rB) {
        B[ty][tx] = WeightH2H.vector[index_3D_gpu(WeightH2H, 0,m*TILE_WIDTH+ty+1, j)];

      } else {
        B[ty][tx] = 0;
      }
      __syncthreads();
      #pragma unroll
      for (int k = 0; k < TILE_WIDTH; ++k) {
        SumThread += A[ty][k] * B[k][tx];
      }
    }
    if (i >= rC || j >= cC) {
      return;
    }

    SumThread += WeightH2H.vector[index_3D_gpu(WeightH2H, 0,0,j)];
    Sum.vector[index_2D_gpu(Sum,i,j)]  = SumThread;
    H2H.vector[index_3D_gpu(H2H,0,i,j)] = 1./(1.0 + exp(-SumThread)) ;

  }


  __global__ void kernel1(int * ranpat, struct Vector_2D Sum, struct Vector_3D WeightH2H, struct Vector_3D H2H, int * nupl, int h){
    int nupl0 = nupl[h];
    int nupl1 = nupl[h+1];
    int bx = blockIdx.x, by = blockIdx.y,
    tx = threadIdx.x, ty = threadIdx.y,
    i = by * TILE_WIDTH + ty,
    j = bx * TILE_WIDTH + tx +1;




    __shared__ REAL A[TILE_WIDTH][TILE_WIDTH];
    __shared__ REAL B[TILE_WIDTH][TILE_WIDTH];
    int rA = *NumPattern_gpu+1;
    int cA = nupl0+1;
    int rB = nupl0+1;
    int cB = nupl1+1;
    int rC = *NumPattern_gpu;
    int cC = nupl1+1;


    REAL SumThread = 0;


    for (int m = 0; m < (cA-1)/TILE_WIDTH+1; ++m) {
      if (i<rA && m*TILE_WIDTH+tx+1 < cA) {
        A[ty][tx] = H2H.vector[index_3D_gpu(H2H,h-1,i,m*TILE_WIDTH+tx+1)];
      } else {
        A[ty][tx] = 0;
      }


      if (j<cB && m*TILE_WIDTH+ty+1 < rB) {
        B[ty][tx] = WeightH2H.vector[index_3D_gpu(WeightH2H, h,m*TILE_WIDTH+ty+1, j)];

      } else {
        B[ty][tx] = 0;
      }
      __syncthreads();
      #pragma unroll
      for (int k = 0; k < TILE_WIDTH; ++k) {
        SumThread += A[ty][k] * B[k][tx];
      }
    }
    if (i >= rC || j >= cC) {
      return;
    }

    SumThread += WeightH2H.vector[index_3D_gpu(WeightH2H, h,0,j)];
    Sum.vector[index_2D_gpu(Sum,i,j)]  = SumThread;
    H2H.vector[index_3D_gpu(H2H,h,i,j)] = 1./(1.0 + exp(-SumThread)) ;

  }




  __device__ inline void myAtomicAdd(float *address, float val)  {
    int i_val = __float_as_int(val);
    int tmp0 = 0;
    int tmp1;
    while( (tmp1 = atomicCAS((int *)address, tmp0, i_val)) != tmp0)
    {
      tmp0 = tmp1;
      i_val = __float_as_int(val + __int_as_float(tmp1));
    }
  }

  __global__ void plus_reduce(REAL *input, int N, REAL *total)
  {
    int tid = threadIdx.x;
    int i = blockIdx.x*blockDim.x + threadIdx.x;

    // Each block loads its elements into shared memory
    __shared__ REAL x[THREADSPERBLOCK];
    x[tid] = (i<N) ? input[i] : 0; // last block may pad with 0’s
    __syncthreads();
    // Build summation tree over elements.
    for(int s=blockDim.x/2; s>0; s=s/2)
    {
      if(tid < s) x[tid] += x[tid + s];
      __syncthreads();
    }
    // Thread 0 adds the partial sum to the total sum
    if( tid == 0 ) {
      myAtomicAdd(total, x[tid]);
    }
  }

  __global__ void kernel2(int * ranpat, struct Vector_2D Sum, struct Vector_2D WeightHO, struct Vector_3D H2H,struct Vector_2D Target, struct Vector_2D Output, struct Vector_2D DeltaO,  int * nupl, REAL * Error, REAL * Error_perP){
    int NumHL = *NumHL_gpu;
    int size_j = nupl[NumHL];
    int size_k = nupl[NumHL+1];
    extern __shared__ REAL WeightHO_TMP[];
    REAL Sumpj=0., Target_temp, Output_temp;
    int np = threadIdx.x + blockIdx.x*blockDim.x;
    int p,j,k;


    __syncthreads();
    #pragma unroll 2
    for( j = 0 ; j <= size_j; j++ ) {
      for( k = 1+threadIdx.x ; k <= size_k ; k+=blockDim.x ) {
        WeightHO_TMP[j*(size_k+1)+k]= WeightHO.vector[index_2D_gpu(WeightHO,j,k)] ;
      }
    }
    __syncthreads();
    if(np >= *NumPattern_gpu){
      return;
    }
    p = ranpat[np];
    Error_perP[np] = 0.;
    REAL my_error = 0.;

    for( k = 1 ; k <= size_k ; k++ ) {    /* compute output unit activations and errors */
      Sumpj = WeightHO_TMP[k];
      //Sumpj = WeightHO.vector[index_2D_gpu(WeightHO,0,k)] ;
      #pragma unroll
      for( j = 1 ; j <= size_j ; j++ ) {
        Sumpj += H2H.vector[index_3D_gpu(H2H,NumHL-1,p,j)] * WeightHO_TMP[(j)*(size_k+1)+k];
        //Sumpj += H2H.vector[index_3D_gpu(H2H,NumHL-1,p,j)] * WeightHO.vector[index_2D_gpu(WeightHO,j,k)] ;
      }
      // maybe useless
      Sum.vector[index_2D_gpu(Sum,p,k)] = Sumpj;
      Output_temp = 1.0/(1.0 + exp(-Sumpj)) ;   /* Sigmoidal Outputs *//* Compute the sigmoid of all the elements of SumO */
      Output.vector[index_2D_gpu(Output,p,k)] = Output_temp;

      Target_temp = Target.vector[index_2D_gpu(Target,p,k-1)];

      //unfortunately, this simple atomicAdd is too heavy for computation times.
      //myAtomicAdd(Error, 0.5 * (Target_temp - Output_temp)*(Target_temp - Output_temp));
      //Error_perP[np] += 0.5 * (Target_temp - Output_temp)*(Target_temp - Output_temp);
      my_error += (Target_temp - Output_temp)*(Target_temp - Output_temp) * 0.5;
      DeltaO.vector[index_2D_gpu(DeltaO,p,k)] = (Target_temp - Output_temp) * Output_temp * (1.0 - Output_temp) ;   /* Sigmoidal Outputs, SSE */
    }
    Error_perP[np] = my_error;
    __syncthreads();

  }

  __global__ void kernel3(int * ranpat, struct Vector_2D Sum, struct Vector_2D WeightHO, struct Vector_3D H2H, struct Vector_2D DeltaO,  int * nupl, struct Vector_3D DeltaH2H, struct Vector_3D WeightH2H){
    int NumHL = *NumHL_gpu;
    int nupl1;
    int size_j = nupl[NumHL];
    int size_k = nupl[NumHL+1];
    extern __shared__ REAL WeightHO_TMP[];
    REAL SumDOWJ=0.;
    int np = threadIdx.x + blockIdx.x*blockDim.x;
    if(np >= *NumPattern_gpu){
      return;
    }
    int p,j,k,h;
    p = ranpat[np];

    #pragma unroll 2
    for( j = 0 ; j <= size_j; j++ ) {
      for( k = 1+threadIdx.x ; k <= size_k ; k+=blockDim.x ) {
        WeightHO_TMP[j*(size_k+1)+k]= WeightHO.vector[index_2D_gpu(WeightHO,j,k)] ;
      }
    }
    if(np >= *NumPattern_gpu){
      return;
    }
    __syncthreads();

    h = NumHL;
    nupl1=nupl[h+1];
    for( j = 1 ; j <= nupl[h] ; j++ ) {
      SumDOWJ = 0.0;  // sumDow is not recommended, has no role and increments comp time
      #pragma unroll
      for( k = 1 ; k <= nupl1 ; k++ ) {
        SumDOWJ += WeightHO_TMP[j*(size_k+1)+k] *  (DeltaO.vector[index_2D_gpu(DeltaO,p,k)]);
      }
      DeltaH2H.vector[index_3D_gpu(DeltaH2H,h-1,p,j)] = SumDOWJ * H2H.vector[index_3D_gpu(H2H,h-1,p,j)] * (1.0 - H2H.vector[index_3D_gpu(H2H,h-1,p,j)]) ;
    }

    __syncthreads();


    for(h=NumHL-1; h>0; h--) {
      nupl1=nupl[h+1];
      for( j = 1 ; j <= nupl[h] ; j++ ) {
        SumDOWJ = 0.0;
        //#pragma  omp reduction (+:SumDOWJ)
        #pragma unroll
        for( k = 1 ; k <= nupl1 ; k++ ) {
          SumDOWJ += (WeightH2H.vector[index_3D_gpu(WeightH2H,h-1,j,k)]) * (DeltaH2H.vector[index_3D_gpu(DeltaH2H,h,p,k)]);
        }
        DeltaH2H.vector[index_3D_gpu(DeltaH2H,h-1,p,j)] = SumDOWJ * H2H.vector[index_3D_gpu(H2H,h-1,p,j)] * (1.0 - H2H.vector[index_3D_gpu(H2H,h-1,p,j)]) ;//t96,block234
      }
    }
  }


  __global__ void kernel4(int * ranpat, struct Vector_2D DeltaWeightHO, struct Vector_3D H2H, struct Vector_2D DeltaO, int * nupl, REAL alpha, REAL eta){
    int bx = blockIdx.x, by = blockIdx.y,
    tx = threadIdx.x, ty = threadIdx.y,
    k = by * TILE_WIDTH + ty +1,
    j = bx * TILE_WIDTH + tx;
    int NumHL = *NumHL_gpu;
    int p,np;
    int NumPattern = *NumPattern_gpu;


    REAL actual; // better for vectorization of inner loop
    //#pragma  omp for private(actual, np)
    if (j == 0 && k <= nupl[NumHL+1]){
      //for( k = 1 ; k <= nupl[NumHL+1] ; k ++ ) {
      actual = alpha * DeltaWeightHO.vector[index_2D_gpu(DeltaWeightHO,0,k)];
      //#pragma  omp reduction (+:actual)
      #pragma unroll
      for( np = 0 ; np < NumPattern ; np++ ) {
        actual += eta * DeltaO.vector[index_2D_gpu(DeltaO,ranpat[np],k)];
      }
      DeltaWeightHO.vector[index_2D_gpu(DeltaWeightHO,0,k)] = actual;
    }

    if(j>0 && j<=nupl[NumHL] && k<= nupl[NumHL+1]){
      //for( j = 1 ; j <= nupl[NumHL] ; j++ ) {
      //  for( k = 1 ; k <= nupl[NumHL+1] ; k ++ ) {
      actual = alpha * DeltaWeightHO.vector[index_2D_gpu(DeltaWeightHO,j,k)];
      //#pragma  omp reduction (+:actual)
      #pragma unroll
      for( np = 0 ; np < NumPattern ; np++ ) {
        p = ranpat[np];
        actual += eta * H2H.vector[index_3D_gpu(H2H,NumHL-1,p,j)] * DeltaO.vector[index_2D_gpu(DeltaO,p,k)]; /* Matrix DeltaWeightHO = eta x (Trasposte of Hidden_Matrix) x DeltaO */
      }
      DeltaWeightHO.vector[index_2D_gpu(DeltaWeightHO,j,k)] = actual;
    }

  }




  __global__ void kernel5(int * ranpat, struct Vector_3D DeltaWeightH2H, int * nupl, REAL alpha, REAL eta, int h){
    int bx = blockIdx.x, by = blockIdx.y,
    tx = threadIdx.x, ty = threadIdx.y,
    j = by * TILE_WIDTH + ty +1,
    i = bx * TILE_WIDTH + tx;
    int i_max = nupl[h-1];
    int j_max = nupl[h];

    //for( i = 0 ; i <= nupl[h-1]; i++ ) {
    //  for( j = 1 ; j <=  nupl[h]; j++ ) {
    #pragma unroll
    if(j>0 && j<=j_max && i<= i_max){
      DeltaWeightH2H.vector[index_3D_gpu(DeltaWeightH2H,h-1,i,j)] *=alpha;
    }
  }


  __global__ void kernel6(int * ranpat, struct Vector_3D DeltaWeightH2H, int * nupl, struct Vector_3D DeltaH2H, REAL eta, int h){
    int np;
    int j_max = nupl[h];
    int j =threadIdx.x + blockIdx.x*blockDim.x +1;
    if(j> j_max){
      return;
    }
    REAL my_Sum = 0.;
    #pragma unroll
    for (np=0; np<*NumPattern_gpu; np++){
       my_Sum += DeltaH2H.vector[index_3D_gpu(DeltaH2H,h-1,ranpat[np],j)];
    }
    DeltaWeightH2H.vector[index_3D_gpu(DeltaWeightH2H,h-1,0,j)] += my_Sum*eta;
  }


  //h=1
  __global__ void kernel7(int * ranpat, struct Vector_3D H2H, struct Vector_2D Input, struct Vector_3D DeltaWeightH2H, struct Vector_3D DeltaH2H, int * nupl, REAL eta){
    int bx = blockIdx.x, by = blockIdx.y,
    tx = threadIdx.x, ty = threadIdx.y,
    j = by * TILE_WIDTH + ty +1,
    i = bx * TILE_WIDTH + tx+1;
    int i_max, j_max, np,p;
    i_max = nupl[0];
    j_max = nupl[1];

    if (i==0 || i> i_max || j==0 || j > j_max){
      return;
    }
    REAL my_Sum = 0.;
    #pragma unroll
    for( np = 0 ; np < *NumPattern_gpu ; np++ ) {
      //ciclo_32f++;
      p = ranpat[np];

      //  for( i = 1 ; i <= i_max ; i++ ) {
      //    for( j = 1 ; j <= j_max ; j++ ) {
      my_Sum +=Input.vector[index_2D_gpu(Input,p,i-1)] * DeltaH2H.vector[index_3D_gpu(DeltaH2H,0,p,j)]; /* Matrix DeltaWeightIH = eta x (Trasposte of Input_Matrix) x DeltaH */
    }
    __syncthreads();
    DeltaWeightH2H.vector[index_3D_gpu(DeltaWeightH2H,0,i,j)] += my_Sum*eta;
  }



  __global__ void kernel8(int * ranpat, struct Vector_3D H2H, struct Vector_2D Input, struct Vector_3D DeltaWeightH2H, struct Vector_3D DeltaH2H, int * nupl, REAL eta, int h){
    int bx = blockIdx.x, by = blockIdx.y,
    tx = threadIdx.x, ty = threadIdx.y,
    j = by * TILE_WIDTH + ty +1,
    i = bx * TILE_WIDTH + tx+1;
    int i_max, j_max, np,p;
    i_max = nupl[h-1];
    j_max = nupl[h];

    if (i==0 || i> i_max || j==0 || j > j_max){
      return;
    }
    REAL my_Sum = 0.;
    #pragma unroll
    for( np = 0 ; np < *NumPattern_gpu ; np++ ) {
      //ciclo_32f++;
      p = ranpat[np];
      //  for( i = 1 ; i <= i_max ; i++ ) {
      ////#pragma  omp simd
      //    for( j = 1 ; j <= j_max ; j++ ) {
      my_Sum += H2H.vector[index_3D_gpu(H2H,h-2,p,i)] * DeltaH2H.vector[index_3D_gpu(DeltaH2H,h-1,p,j)]; /* Matrix DeltaWeightIH = eta x (Trasposte of Input_Matrix) x DeltaH */
    }
    __syncthreads();
    DeltaWeightH2H.vector[index_3D_gpu(DeltaWeightH2H,h-1,i,j)] += my_Sum*eta;
  }

  __global__ void kernel9(int * ranpat, struct Vector_2D WeightHO, struct Vector_2D DeltaWeightHO, int * nupl){
    int bx = blockIdx.x, by = blockIdx.y,
    tx = threadIdx.x, ty = threadIdx.y,
    j = by * TILE_WIDTH + ty,
    k = bx * TILE_WIDTH + tx+1;

    if(k>*NumOutput_gpu || j>nupl[*NumHL_gpu] || k< 1 || j<0){
      return;
    }

    WeightHO.vector[index_2D_gpu(WeightHO,j,k)] += DeltaWeightHO.vector[index_2D_gpu(DeltaWeightHO,j,k)] ;


  }

  __global__ void kernel10(int * ranpat, struct Vector_3D WeightH2H, struct Vector_3D DeltaWeightH2H, int * nupl, int h){
    int bx = blockIdx.x, by = blockIdx.y,
    tx = threadIdx.x, ty = threadIdx.y,
    i = by * TILE_WIDTH + ty,
    j = bx * TILE_WIDTH + tx+1;

    if(j>nupl[h] || i>nupl[h-1] || j< 1 || i<0){
      return;
    }

    WeightH2H.vector[index_3D_gpu(WeightH2H,h-1,i,j)] += DeltaWeightH2H.vector[index_3D_gpu(DeltaWeightH2H,h-1,i,j)];
  }
































  int main(int argc, char *argv[]) {
    TIMER_DEF;
    TIMER_START;
    cudaEvent_t start, stop;
    fprintf(stderr, "START\n");
    int     h, i, j, k, p, np, epoch;
    int    NumPattern, NumInput, NumOutput;
    /*
    double Input[NUMPAT+1][NUMIN+1] = { 0, 0, 0,  0, 0, 0,  0, 1, 0,  0, 0, 1,  0, 1, 1 };
    double Target[NUMPAT+1][NUMOUT+1] = { 0, 0,  0, 0,  0, 1,  0, 1,  0, 0 };
    double SumH[NUMPAT+1][NUMHID+1], WeightIH[NUMIN+1][NUMHID+1], Hidden[NUMPAT+1][NUMHID+1];
    double SumO[NUMPAT+1][NUMOUT+1], WeightHO[NUMHID+1][NUMOUT+1], Output[NUMPAT+1][NUMOUT+1];
    double DeltaO[NUMPAT+1][NUMOUT+1], SumDOW[NUMHID+1], DeltaH[NUMPAT+1][NUMHID+1];
    double DeltaWeightIH[NUMIN+1][NUMHID+1], DeltaWeightHO[NUMHID+1][NUMOUT+1];
    double Error, eta = 0.5, alpha = 0.9, smallwt = 0.5;
    */
    struct Vector_2D Input, Input_gpu;
    struct Vector_2D Target, Target_gpu;
    struct Vector_2D Sum, Sum_gpu,Sum_gpu_2;
    struct Vector_3D H2H, H2H_gpu,H2H_gpu_2, DeltaH2H, DeltaH2H_gpu;
    struct Vector_3D WeightH2H, WeightH2H_gpu, DeltaWeightH2H, DeltaWeightH2H_gpu;
    struct Vector_2D WeightHO, WeightHO_gpu, Output, Output_gpu;
    struct Vector_2D DeltaO, DeltaO_gpu;
    struct Vector_2D DeltaWeightHO, DeltaWeightHO_gpu;
    REAL Error, Eps, eta, alpha, smallwt;
    REAL *Error_gpu, *Error_perP_gpu;
    int *ranpat, * ranpat_gpu;
    int verbose=FALSE;
    int maxepoch=DEFMAXEPOCH;
    int dimsum=0;
    int NumHL=1;
    int *nupl=NULL, *nupl_gpu;
    char *inputfile = NULL;
    char *po;
    dictionary *ini;
    char key[MAXSTRLEN];
    char formatstring[MAXSTRLEN];
    char LogFileName[MAXSTRLEN];
    char InputFileName[MAXSTRLEN];
    char TargetFileName[MAXSTRLEN];
    char ResultFileName[MAXSTRLEN];
    char RestartFileName[MAXSTRLEN];
    char DeltaFileName[MAXSTRLEN];
    char RestartDeltaFileName[MAXSTRLEN];
    FILE *fp=NULL;
    FILE *fpd=NULL;
    FILE *fpl=NULL;

    // TO DELETE
    //int ciclo_32a = 0, ciclo_32b = 0, ciclo_32c  = 0, ciclo_32d  = 0, ciclo_32e  = 0, ciclo_32f = 0,
    int i_max, j_max, nupl1;
    REAL * pointer32;
    REAL * DeltaH2H_h1_p;
    REAL * factor_x;
    REAL factor_x_indexed;
    REAL SumDOWJ, Sumpj;
    REAL Target_temp,Output_temp;
    REAL * factor_delta, **factor_weight;

    if(sizeof(REAL)==sizeof(float)) {
      strcpy(formatstring,"%f ");
    }
    if(sizeof(REAL)==sizeof(double)) {
      strcpy(formatstring,"%lf ");
    }

    for(i = 1; i < argc; i++) {
      po = argv[i];
      if (*po++ == '-') {
        switch (*po++) {
          case 'h':
          Usage(argv[0]);
          exit(OK);
          break;
          case 'v':
          verbose=TRUE;
          break;
          case 'i':
          SKIPBLANK
          inputfile=Strdup(po);
          break;
          default:
          Usage(argv[0]);
          exit(OK);
          break;
        }
      }
    }
    if(inputfile==NULL) {
      Usage(argv[0]);
      exit(OK);
    }

    ini = iniparser_load(inputfile);

    if(ini==NULL) { writelog(TRUE,APPLICATION_RC,"Cannot parse file: %s\n", inputfile); }

    READINTFI(maxepoch,"Max number of epochs");
    READINTFI(NumPattern,"Number of training data");
    READINTFI(NumInput,"Number of input units");
    READINTFI(NumOutput,"Number of output units");
    READINTFI(NumHL,"Number of hidden layers");
    READREALFI(eta,"Learning rate");
    READREALFI(alpha,"Momentum");
    READREALFI(smallwt,"Initialization scale");
    READREALFI(Eps,"Error threshold");
    {READSTRFI(LogFileName,"Log file name");}
    {READSTRFI(InputFileName,"Input file name");}
    {READSTRFI(TargetFileName,"Target file name");}
    {READSTRFI(ResultFileName,"Results file name");}
    {READSTRFI(DeltaFileName,"Result delta file name");}
    {READSTRFI(RestartFileName,"Restart file name");}
    {READSTRFI(RestartDeltaFileName,"Restart delta file name");}
    nupl=(int *)makevect(NumHL+2,sizeof(int));
    nupl[0]=NumInput;
    nupl[NumHL+1]=NumOutput;
    if(NumHL) {
      int scratch;
      char tempstring[MAXSTRLEN];
      //H2H=(REAL ***)Malloc(sizeof(REAL *)*(NumHL));
      //WeightH2H=(REAL ***)Malloc(sizeof(REAL *)*(NumHL));
      //DeltaH2H=(REAL ***)Malloc(sizeof(REAL *)*(NumHL));
      //DeltaWeightH2H=(REAL ***)Malloc(sizeof(REAL *)*(NumHL));
      int rows[MAX_SIZE_VECTOR];
      int cols[MAX_SIZE_VECTOR];
      for(i=1; i<=NumHL; i++) {
        snprintf(tempstring,sizeof(tempstring),"Number of units in layer %d",i-1);
        READINTFI(scratch,tempstring);
        nupl[i]=scratch;
        rows[i-1] = NumPattern;
        cols[i-1] = nupl[i]+1;
        //H2H[i-1]=(REAL **)makematr(NumPattern, nupl[i]+1,sizeof(REAL));
        //DeltaH2H[i-1]=(REAL **)makematr(NumPattern, nupl[i]+1,sizeof(REAL));
      }
      H2H = malloc_3D(NumHL, rows, cols);
      H2H_gpu_2 = malloc_3D(NumHL, rows, cols);
      DeltaH2H = malloc_3D(NumHL, rows, cols);

      for(i=1; i<=NumHL; i++) {
        rows[i-1] = nupl[i-1]+1;
        cols[i-1] = nupl[i]+1;
        //DeltaWeightH2H[i-1]=(REAL **)makematr(nupl[i-1]+1,nupl[i]+1,sizeof(REAL));
        //WeightH2H[i-1]=(REAL **)makematr(nupl[i-1]+1,nupl[i]+1,sizeof(REAL));
      }
      WeightH2H = malloc_3D(NumHL, rows, cols);
      DeltaWeightH2H = malloc_3D(NumHL, rows, cols);
    }

    dimsum=nupl[1];
    for(i=1; i<=(NumHL+1); i++) {
      dimsum=MAX(dimsum,nupl[i]);
    }
    Input=malloc_2D(NumPattern, NumInput);
    //Input=(REAL **)makematr(NumPattern, NumInput,sizeof(REAL));
    Target=malloc_2D(NumPattern, NumOutput);
    //Target=(REAL **)makematr(NumPattern, NumOutput,sizeof(REAL));
    Sum=malloc_2D(NumPattern, dimsum+1);
    Sum_gpu_2=malloc_2D(NumPattern, dimsum+1);

    Output=malloc_2D(NumPattern, NumOutput+1);
    DeltaO=malloc_2D(NumPattern, NumOutput+1);
    WeightHO=malloc_2D(nupl[NumHL]+1, NumOutput+1);
    DeltaWeightHO=malloc_2D(nupl[NumHL]+1, NumOutput+1);
    ranpat=(int *) makevect(NumPattern,sizeof(int));


    #if defined(TAKETIME)
    LAP(0.);
    #endif
    ReadFromFile(InputFileName,Input,NumPattern,NumInput,sizeof(REAL));

    ReadFromFile(TargetFileName,Target,NumPattern,NumOutput,sizeof(REAL));

    //#pragma  omp task

    if(strcmp(LogFileName,NULLFILE)) { fpl=Fopen(LogFileName,"w"); }

    for( k = 0 ; k <= nupl[NumHL+1] ; k ++ ) {    /* initialize WeightHO and DeltaWeightHO */
      for( j = 0 ; j <= nupl[NumHL] ; j++ ) {
        DeltaWeightHO.vector[index_2D(DeltaWeightHO,j,k)] = 0.0 ;
        WeightHO.vector[index_2D(WeightHO,j,k)] = 2.0 * ( drand48() - 0.5 ) * smallwt ;
      }
    }
    for(h=NumHL; h>0; h--) {
      for(j=0; j<=nupl[h]; j++) {
        for(i=0; i<=nupl[h-1]; i++) {
          DeltaWeightH2H.vector[index_3D(DeltaWeightH2H,h-1,i,j)] = 0.0 ;
          WeightH2H.vector[index_3D(WeightH2H,h-1,i,j)] = 2.0 * ( drand48() - 0.5 ) * smallwt ;
        }
      }
    }
    if(strcmp(RestartFileName,NULLFILE)) {
      fp=Fopen(RestartFileName,"r");
      if(strcmp(RestartDeltaFileName,NULLFILE)) {
        fpd=Fopen(RestartDeltaFileName,"r");
      }
      for( k = 1 ; k <= nupl[NumHL+1] ; k ++ ) {
        fscanf(fp,formatstring,&(WeightHO.vector[index_2D(WeightHO,0,k)]));
        if(fpd) fscanf(fpd,formatstring,&(DeltaWeightHO.vector[index_2D(DeltaWeightHO,0,k)]));
        for( j = 1 ; j <= nupl[NumHL] ; j++ ) {
          fscanf(fp,formatstring,&(WeightHO.vector[index_2D(WeightHO,j,k)]));
          if(fpd) fscanf(fpd,formatstring,&(DeltaWeightHO.vector[index_2D(DeltaWeightHO,j,k)]));
        }
        fscanf(fp,"\n");
        if(fpd) fscanf(fpd,"\n");
      }

      for(h=NumHL; h>0; h--) {
        for( j = 1 ; j <= nupl[h] ; j++ ) {
          fscanf(fp,formatstring,&(WeightH2H.vector[index_3D(WeightH2H,h-1,0,j)]));
          if(fpd) fscanf(fpd,formatstring,&(DeltaWeightH2H.vector[index_3D(DeltaWeightH2H,h-1,0,j)]));
          for( i = 1 ; i <= nupl[h-1] ; i++ ) {
            fscanf(fp,formatstring,&(WeightH2H.vector[index_3D(WeightH2H,h-1,i,j)]));
            if(fpd) fscanf(fpd,formatstring,&(DeltaWeightH2H.vector[index_3D(DeltaWeightH2H,h-1,i,j)]));
          }
          fscanf(fp,"\n");
          if(fpd)fscanf(fpd,"\n");
        }
      }

      if (fp) fclose(fp);
      if (fpd) fclose(fpd);
    }

    #if defined(TAKETIME)
    LAP(1.);
    #endif

    if(verbose) {
      printf("\nInitial Bias and Weights\n");
      for( k = 1 ; k <=  nupl[NumHL+1] ; k ++ ) {
        printf("Bias H to O[%d]: %f\n",k,WeightHO.vector[index_2D(WeightHO,0,k)]);
        for( j = 1 ; j <= nupl[NumHL] ; j++ ) {
          printf("Weight H[%d] to O[%d]: %f\n",j,k,WeightHO.vector[index_2D(WeightHO,j,k)]);
        }
      }
      for(h=NumHL; h>0; h--) {
        for( j = 1 ; j <= nupl[h] ; j++ ) {
          printf("Bias[%d][%d]: %f\n",h-1,j,WeightH2H.vector[index_3D(WeightH2H,h-1,0,j)]);
          for( i = 1 ; i <= nupl[h-1] ; i++ ) {
            printf("Weight[%d][%d][%d]: %f\n",h-1,i,j,WeightH2H.vector[index_3D(WeightH2H,h-1,i,j)]);
          }
        }
      }
    }
    #if defined(TAKETIME)
    LAP(1.5);
    fprintf(stdout, "loading memory on gpu\n");
    #endif

    for( p = 0 ; p < NumPattern ; p++ ) {    /* initialize order of individuals */
      ranpat[p] = p ;
    }
    /*

    END OF INITIALIZATION

    */




    // for kernel 0
    Sum_gpu = copy2DToGPU(Sum);
    WeightH2H_gpu = copy3DToGPU(WeightH2H);
    Input_gpu = copy2DToGPU(Input);
    H2H_gpu = copy3DToGPU(H2H);
    ranpat_gpu =(int *) copy1DToGPU((void *)ranpat, NumPattern * sizeof(int));
    Error_gpu =(REAL *) copy1DToGPU((void *)&Error, sizeof(Error));
    CUDA_CHECK(cudaMalloc((void **)&Error_perP_gpu, sizeof(REAL)*NumPattern));
    nupl_gpu = (int *)copy1DToGPU((void *)nupl, (NumHL+2) * sizeof(int));
    CUDA_CHECK(cudaMemcpyToSymbol(NumPattern_gpu, &NumPattern, sizeof(int))); //this is a constant
    CUDA_CHECK(cudaMemcpyToSymbol(NumHL_gpu, &NumHL, sizeof(int))); //this is a constant
    CUDA_CHECK(cudaMemcpyToSymbol(NumOutput_gpu, &NumOutput, sizeof(int))); //this is a constant


    // for kernel 1
    WeightHO_gpu = copy2DToGPU(WeightHO);
    Target_gpu = copy2DToGPU(Target);
    Output_gpu = copy2DToGPU(Output);
    DeltaO_gpu = copy2DToGPU(DeltaO);

    //for kernel 2
    DeltaH2H_gpu = copy3DToGPU(DeltaH2H);

    //for kernel 3
    DeltaWeightHO_gpu = copy2DToGPU(DeltaWeightHO);

    // for kernel 4
    DeltaWeightH2H_gpu = copy3DToGPU(DeltaWeightH2H);


    #if defined(TAKETIME)
    LAP(1.55);
    fprintf(stdout, "Done!\n");
    #endif


    int nuplh,nuplh1;
















    // main loop
    for( epoch = 0 ; epoch < maxepoch ; epoch++) {    /* iterate weight updates */

      #if defined(TAKETIME)
      LAP(2.1);
      #endif

      Error = 0.0 ;
      Error_gpu =(REAL *) copy1DToGPU((void *)&Error, sizeof(Error));

      if(100>10){

        CUDA_CHECK(cudaEventCreate(&start));
        CUDA_CHECK(cudaEventCreate(&stop));
        CUDA_CHECK(cudaEventRecord(start, 0));
        /*struct cudaDeviceProp properties;
        cudaGetDeviceProperties(&properties, 0);
        fprintf(stderr, "multiProcessorCount %d\n",properties.multiProcessorCount);
        fprintf(stderr, "maxThreadsPerMultiProcessor %d\n",properties.maxThreadsPerMultiProcessor);
        */
        int rows = NumPattern; int cols = nupl[1];

        dim3 grid((cols-1)/TILE_WIDTH+1, (rows-1)/TILE_WIDTH+1, 1);
        dim3 block(TILE_WIDTH, TILE_WIDTH, 1);
        kernel0<<<grid,block>>>(ranpat_gpu, Sum_gpu, WeightH2H_gpu, Input_gpu, H2H_gpu, nupl_gpu, 0);
        CUDA_CHECK_KERNEL;

        copy2DToCPU(Sum_gpu, Sum);
        copy3DToCPU(H2H_gpu,H2H);
        cudaDeviceSynchronize();

        //copy3DToCPU(WeightH2H_gpu,WeightH2H);
        CUDA_CHECK(cudaEventRecord(stop, 0));
        CUDA_CHECK(cudaEventSynchronize(stop));
        float inc = 0;
        CUDA_CHECK(cudaEventElapsedTime(&inc, start, stop));
        printf("Time kernel 0 (ms): %f\n", inc);


        /*
        for (int zz= 0; zz<10;zz++){
        for (int zzi= 0; zzi<57;zzi++){
        if(H2H.vector[index_3D(H2H,0,zz,zzi)]!=H2H_gpu_2.vector[index_3D(H2H,0,zz,zzi)]){
        printf("h%f-%f,",H2H.vector[index_3D(H2H,0,zz,zzi)],H2H_gpu_2.vector[index_3D(H2H,0,zz,zzi)]);
      }
      if(Sum.vector[index_2D(Sum,zz,zzi)]!=Sum_gpu_2.vector[index_2D(Sum,zz,zzi)]){
      printf("S%f-%f,",Sum.vector[index_2D(Sum,zz,zzi)],Sum_gpu_2.vector[index_2D(Sum,zz,zzi)]);
    }
  }

}
printf("\n\n");
*/

}
else{

  nuplh1 = nupl[0+1];
  nuplh = nupl[0];
  //#pragma  omp parallel for schedule(static) private(np,j, k,Sumpj, SumDOWJ, p, Target_temp,Output_temp, factor_delta, factor_weight, nupl1) firstprivate(h, nuplh,nuplh1)
  for( np = 0 ; np < NumPattern ; np++ ) {
    p = ranpat[np];


    // great refactor, two loops in one , for-h is been pulled out to optimize read speed.
    // it's faster than 2 loops

    ////#pragma  omp simd
    for(k=1; k<=nuplh1; k++) {
      Sum.vector[index_2D(Sum,p,k)] =  WeightH2H.vector[index_3D(WeightH2H,0,0,k)] ;
    }


    for( j = 1 ; j <= nuplh ; j++ ) {
      //Sumpj = (h>0)? H2H[h-1][p][j] : Input[p][j-1]; //not so efficient as an if of the whole for
      Sumpj = Input.vector[index_2D(Input,p,j-1)];
      //Sumpj = WeightH2H[h][0][k];
      ////#pragma  omp simd
      for(k=1; k<=nuplh1; k++) {
        Sum.vector[index_2D(Sum,p,k)] += Sumpj * WeightH2H.vector[index_3D(WeightH2H,0,j,k)] ;
        /* Matrix SumO = Hidden_Matrix x Weight_Output Matrix
        The Hidden_Matrix has one row per sample
        The Weight_Output_Matrix has one row per number of neurons in the hidden level
        The SumO Matrix is initialized with the Bias */
      }
      //H2H[h][p][k] = 1.0/(1.0 + exp(-Sumpj)) ;   /* Sigmoidal Outputs *//* Compute the sigmoid of all the elements of SumO */
    }


    // loop is been inverted, so k is inner and j outer

    ////#pragma  omp simd
    for(k=1; k<=nuplh1; k++) {
      // this line is a little bit lighter, Sum is not inverted, it's already negative
      H2H.vector[index_3D(H2H,0,p,k)] = 1./(1.0 + exp(-Sum.vector[index_2D(Sum,p,k)])) ;
    }
  }

  /*for (int zz= 0; zz<60000;zz++){
  for (int zzi= 0; zzi<57;zzi++){
  if((REAL)H2H.vector[index_3D(H2H,0,zz,zzi)]!=(REAL)H2H_gpu_2.vector[index_3D(H2H_gpu_2,0,zz,zzi)]){
  printf("h %.9lf/%.9lf, ",H2H.vector[index_3D(H2H,0,zz,zzi)],H2H_gpu_2.vector[index_3D(H2H_gpu_2,0,zz,zzi)]);
}
if((REAL)Sum.vector[index_2D(Sum,zz,zzi)]!=(REAL)Sum_gpu_2.vector[index_2D(Sum_gpu_2,zz,zzi)]){
printf("S %.9lf/%.9lf, ",Sum.vector[index_2D(Sum,zz,zzi)],Sum_gpu_2.vector[index_2D(Sum_gpu_2,zz,zzi)]);
}
}

}
printf("\n\n");
*/


}

for( h=1; h<NumHL; h++) {
  if(100>10){

    CUDA_CHECK(cudaEventCreate(&start));
    CUDA_CHECK(cudaEventCreate(&stop));
    CUDA_CHECK(cudaEventRecord(start, 0));
    /*struct cudaDeviceProp properties;
    cudaGetDeviceProperties(&properties, 0);
    fprintf(stderr, "multiProcessorCount %d\n",properties.multiProcessorCount);
    fprintf(stderr, "maxThreadsPerMultiProcessor %d\n",properties.maxThreadsPerMultiProcessor);
    */
    int rows = NumPattern; int cols = nupl[1];

    dim3 grid((cols-1)/TILE_WIDTH+1, (rows-1)/TILE_WIDTH+1, 1);
    dim3 block(TILE_WIDTH, TILE_WIDTH, 1);
    kernel1<<<grid,block>>>(ranpat_gpu, Sum_gpu, WeightH2H_gpu, H2H_gpu, nupl_gpu, h);
    CUDA_CHECK_KERNEL;

    copy2DToCPU(Sum_gpu, Sum);
    copy3DToCPU(H2H_gpu,H2H);

    //copy3DToCPU(WeightH2H_gpu,WeightH2H);
    CUDA_CHECK(cudaEventRecord(stop, 0));
    CUDA_CHECK(cudaEventSynchronize(stop));
    float inc = 0;
    CUDA_CHECK(cudaEventElapsedTime(&inc, start, stop));
    printf("Time of kernel 1 (ms): %f\n", inc);
  }else{


    nuplh1 = nupl[h+1];
    nuplh = nupl[h];
    //#pragma  omp parallel for schedule(static) private(np,j, k,Sumpj, SumDOWJ, p, Target_temp,Output_temp, factor_delta, factor_weight, nupl1) firstprivate(h, nuplh,nuplh1)
    for( np = 0 ; np < NumPattern ; np++ ) {
      p = ranpat[np];


      // great refactor, two loops in one , for-h is been pulled out to optimize read speed.
      // it's faster than 2 loops

      ////#pragma  omp simd
      for(k=1; k<=nuplh1; k++) {
        Sum.vector[index_2D(Sum,p,k)] =  WeightH2H.vector[index_3D(WeightH2H,h,0,k)] ;
      }



      for( j = 1 ; j <= nuplh ; j++ ) {
        Sumpj = H2H.vector[index_3D(H2H,h-1,p,j)];
        //Sumpj = WeightH2H[h][0][k];
        ////#pragma  omp simd
        for(k=1; k<=nuplh1; k++) {
          //Sumpj +=  factor_matrix_or_input[((h>0)?j:j-1)] * WeightH2H[h][j][k] ;
          Sum.vector[index_2D(Sum,p,k)] += Sumpj * WeightH2H.vector[index_3D(WeightH2H,h,j,k)];
          //Sum[p][j] += Input[p][j-1] * WeightH2H[0][i][j] ; h==0
          /* Matrix SumO = Hidden_Matrix x Weight_Output Matrix
          The Hidden_Matrix has one row per sample
          The Weight_Output_Matrix has one row per number of neurons in the hidden level
          The SumO Matrix is initialized with the Bias */
        }
      }

      // loop is been inverted, so k is inner and j outer

      ////#pragma  omp simd
      for(k=1; k<=nuplh1; k++) {
        // this line is a little bit lighter, Sum is not inverted, it's already negative
        H2H.vector[index_3D(H2H,h,p,k)] = 1./(1.0 + exp(-Sum.vector[index_2D(Sum,p,k)])) ;
      }
    }
  }


}
REAL my_error;
#if defined(TAKETIME)
LAP(2.12);
#endif

if(10>1){
  CUDA_CHECK(cudaEventCreate(&start));
  CUDA_CHECK(cudaEventCreate(&stop));
  CUDA_CHECK(cudaEventRecord(start, 0));
  REAL* appoggio = (REAL *)malloc(NumPattern*sizeof(REAL));

  int size_j = nupl[NumHL];
  int size_k = nupl[NumHL+1];
  kernel2<<<NumPattern/THREADSPERBLOCK+1,THREADSPERBLOCK, (size_j+1)*(size_k+1)*sizeof(REAL)>>>(ranpat_gpu, Sum_gpu, WeightHO_gpu, H2H_gpu, Target_gpu, Output_gpu,DeltaO_gpu, nupl_gpu, Error_gpu, Error_perP_gpu);
  plus_reduce<<<NumPattern/THREADSPERBLOCK+1,THREADSPERBLOCK>>>(Error_perP_gpu, NumPattern, Error_gpu);
  CUDA_CHECK_KERNEL;


  copy2DToCPU(DeltaO_gpu, DeltaO);
  copy2DToCPU(Output_gpu,Output);
  copy1DToCPU((void *)Error_perP_gpu,appoggio, sizeof(REAL)*NumPattern);
  copy1DToCPU((void *) Error_gpu,&Error, sizeof(REAL));
  CUDA_CHECK(cudaEventRecord(stop, 0));
  CUDA_CHECK(cudaEventSynchronize(stop));
  float inc = 0;
  CUDA_CHECK(cudaEventElapsedTime(&inc, start, stop));
  printf("Time of kernel2 (ms): %f\n", inc);
  REAL sum_temp =0.;
  for (int as=0;as<NumPattern;as++){
    //printf("%f-",appoggio[as]);
    sum_temp+= appoggio[as];
  }
  printf("somma %f, in cuda %f\n",sum_temp, Error);

}
if(0>0){

  //#pragma  omp for schedule(static) private(np, Sumpj, p, j, k, h,Output_temp, factor_delta, factor_weight, nupl1, my_error)
  for( np = 0 ; np < NumPattern ; np++ ) {
    my_error = 0;
    p = ranpat[np];
    for( k = 1 ; k <= nupl[NumHL+1] ; k++ ) {    /* compute output unit activations and errors */
      Sumpj = WeightHO.vector[index_2D(WeightHO,0,k)] ;
      //#pragma  omp reduction (+:Sumpj)
      for( j = 1 ; j <= nupl[NumHL] ; j++ ) {
        Sumpj += H2H.vector[index_3D(H2H,NumHL-1,p,j)] * WeightHO.vector[index_2D(WeightHO,j,k)] ;/* Matrix SumO = Hidden_Matrix x Weight_Output Matrix
        The Hidden_Matrix has one row per sample
        The Weight_Output_Matrix has one row per number of neurons in the hidden level
        The SumO Matrix is initialized with the Bias */
      }
      Output_temp = 1.0/(1.0 + exp(-Sumpj)) ;   /* Sigmoidal Outputs *//* Compute the sigmoid of all the elements of SumO */
      /*              printf("Epoch %d, pattern %d, output %d, output %f, target %f\n", epoch, p, k, Output[p][k], Target[p][k-1]); */
      /*              Output[p][k] = SumO[p][k];      Linear Outputs */

      if(Output.vector[index_2D(Output,p,k)] >0.1){
        //if(Output.vector[index_2D(Output,p,k)] - Output_temp > 0.5 || -Output.vector[index_2D(Output,p,k)] + Output_temp > 0.5 || Output.vector[index_2D(Output,p,k)] >0.1){
        printf("p %d k %d cuda %f cpu %f\n", p,k,Output.vector[index_2D(Output,p,k)] , Output_temp);
      }

      Target_temp = Target.vector[index_2D(Target,p,k-1)];
      Output.vector[index_2D(Output,p,k)] = Output_temp;
      // Added temp variables, added pow instead double product
      Error += 0.5 * pow((Target_temp - Output_temp) ,2) ;   /* SSE */
      //Error += 0.5 * (Target[p][k-1] - Output[p][k]) * (Target[p][k-1] - Output[p][k]) ;   /* SSE */

      /*              Error -= ( Target[p][k-1] * log( Output[p][k] ) + ( 1.0 - Target[p][k-1] ) * log( 1.0 - Output[p][k] ) ) ;    Cross-Entropy Error */
      DeltaO.vector[index_2D(DeltaO,p,k)] = (Target_temp - Output_temp) * Output_temp * (1.0 - Output_temp) ;   /* Sigmoidal Outputs, SSE */
      /* derivative of the error x derivative of the sigmoidal function */
      /*              DeltaO[p][k] = Target[p][k-1] - Output[p][k];     Sigmoidal Outputs, Cross-Entropy Error */
      /*              DeltaO[p][k] = Target[p][k-1] - Output[p][k];     Linear Outputs, SSE */
    }
    //#pragma  omp atomic
    //Error += my_error;
  }
}
#if defined(TAKETIME)
//#pragma  omp single
{LAP(2.13);}
#endif

if(10>1){
  CUDA_CHECK(cudaEventCreate(&start));
  CUDA_CHECK(cudaEventCreate(&stop));
  CUDA_CHECK(cudaEventRecord(start, 0));

  int size_j = nupl[NumHL];
  int size_k = nupl[NumHL+1];
  kernel3<<<NumPattern/THREADSPERBLOCK+1,THREADSPERBLOCK, (size_j+1)*(size_k+1)*sizeof(REAL)>>>(ranpat_gpu, Sum_gpu, WeightHO_gpu, H2H_gpu,DeltaO_gpu, nupl_gpu, DeltaH2H_gpu, WeightH2H_gpu);
  CUDA_CHECK_KERNEL;


  copy3DToCPU(DeltaH2H_gpu, DeltaH2H);
  CUDA_CHECK(cudaEventRecord(stop, 0));
  CUDA_CHECK(cudaEventSynchronize(stop));
  float inc = 0;
  CUDA_CHECK(cudaEventElapsedTime(&inc, start, stop));
  printf("Time kernel 3 (ms): %f\n", inc);
}
else{
  for( np = 0 ; np < NumPattern ; np++ ) {
    p = ranpat[np];
    for(h=NumHL; h>0; h--) {
      nupl1=nupl[h+1];
      //factor_delta = (h<NumHL)?DeltaH2H[h][p][k]:DeltaO[p][k]; //before the most probable
      //factor_weight = (h<NumHL)?WeightH2H[h-1][j][k]:WeightHO[j][k]; //before the most probable
      for( j = 1 ; j <= nupl[h] ; j++ ) {
        SumDOWJ = 0.0;  // sumDow is not recommended, has no role and increments comp time
        //#pragma  omp reduction (+:SumDOWJ)
        for( k = 1 ; k <= nupl1 ; k++ ) {
          SumDOWJ += ((h<NumHL)?WeightH2H.vector[index_3D(WeightH2H,h-1,j,k)]:WeightHO.vector[index_2D(WeightHO,j,k)]) * \\
          ((h<NumHL)?DeltaH2H.vector[index_3D(DeltaH2H,h,p,k)]:DeltaO.vector[index_2D(DeltaO,p,k)]);
          //SumDOWJ += WeightHO[j][k] * DeltaO[p][k]; //primo h=NumHL
          //SumDOWJ += WeightH2H[h-1][j][k] * DeltaH2H[h][p][k]; // dopo h<NumHL
        }
        DeltaH2H.vector[index_3D(DeltaH2H,h-1,p,j)] = SumDOWJ * H2H.vector[index_3D(H2H,h-1,p,j)] * (1.0 - H2H.vector[index_3D(H2H,h-1,p,j)]) ;
      }
    }
  }
}



#if defined(TAKETIME)
//#pragma  omp single
{LAP(3.);}
#endif
if(10>1){
  CUDA_CHECK(cudaEventCreate(&start));
  CUDA_CHECK(cudaEventCreate(&stop));
  CUDA_CHECK(cudaEventRecord(start, 0));


  dim3 grid((nupl[NumHL]+1)/TILE_WIDTH+1, (nupl[NumHL+1])/TILE_WIDTH+1, 1);
  dim3 block(TILE_WIDTH, TILE_WIDTH, 1);
  kernel4<<<grid,block>>>(ranpat_gpu, DeltaWeightHO_gpu, H2H_gpu,DeltaO_gpu, nupl_gpu, alpha, eta);
  CUDA_CHECK_KERNEL;
  cudaDeviceSynchronize();


  copy2DToCPU(DeltaWeightHO_gpu, DeltaWeightHO);
  cudaDeviceSynchronize();
  CUDA_CHECK(cudaEventRecord(stop, 0));
  CUDA_CHECK(cudaEventSynchronize(stop));
  float inc = 0;
  CUDA_CHECK(cudaEventElapsedTime(&inc, start, stop));
  printf("Time kernel 4 (ms): %f\n", inc);
}
else{

  REAL actual; // better for vectorization of inner loop
  //#pragma  omp for private(actual, np)
  for( k = 1 ; k <= nupl[NumHL+1] ; k ++ ) {
    actual = alpha * DeltaWeightHO.vector[index_2D(DeltaWeightHO,0,k)];
    //#pragma  omp reduction (+:actual)
    for( np = 0 ; np < NumPattern ; np++ ) {
      actual += eta * DeltaO.vector[index_2D(DeltaO,ranpat[np],k)];
    }
    DeltaWeightHO.vector[index_2D(DeltaWeightHO,0,k)] = actual;
  }


  for( j = 1 ; j <= nupl[NumHL] ; j++ ) {
    for( k = 1 ; k <= nupl[NumHL+1] ; k ++ ) {

      actual = alpha * DeltaWeightHO.vector[index_2D(DeltaWeightHO,j,k)];
      //#pragma  omp reduction (+:actual)
      for( np = 0 ; np < NumPattern ; np++ ) {
        p = ranpat[np];
        actual += eta * H2H.vector[index_3D(H2H,NumHL-1,p,j)] * DeltaO.vector[index_2D(DeltaO,p,k)]; /* Matrix DeltaWeightHO = eta x (Trasposte of Hidden_Matrix) x DeltaO */
        /* DeltaWeight is initialized with the "momentum" */
      }
      DeltaWeightHO.vector[index_2D(DeltaWeightHO,j,k)] = actual;
    }
  }

}


#if defined(TAKETIME)
//#pragma  omp single
{LAP(3.2);}
#endif

//all this part is been refactored. it was the longest part in serial version.
//loops of original version are abcdef, last parallel version is totally different to
//optimize memory access and instructions number
for(h=NumHL; h>0; h--) {
  if (10>0){
    CUDA_CHECK(cudaEventCreate(&start));
    CUDA_CHECK(cudaEventCreate(&stop));
    CUDA_CHECK(cudaEventRecord(start, 0));


    dim3 grid((nupl[h-1]+1)/TILE_WIDTH+1, (nupl[h])/TILE_WIDTH+1, 1);
    dim3 block(TILE_WIDTH, TILE_WIDTH, 1);
    kernel5<<<grid,block>>>(ranpat_gpu, DeltaWeightH2H_gpu, nupl_gpu, alpha, eta, h);
    CUDA_CHECK_KERNEL;


    copy3DToCPU(DeltaWeightH2H_gpu, DeltaWeightH2H);
    CUDA_CHECK(cudaEventRecord(stop, 0));
    CUDA_CHECK(cudaEventSynchronize(stop));
    float inc = 0;
    CUDA_CHECK(cudaEventElapsedTime(&inc, start, stop));
    printf("Time kernel 5 (ms): %f\n", inc);
  }
  else
  {
    //ciclo_32a++;
    i_max = nupl[h-1];
    j_max = nupl[h];
    //#pragma  omp parallel for schedule(static) collapse(2)
    for( i = 0 ; i <= i_max ; i++ ) {
      for( j = 1 ; j <= j_max ; j++ ) {
        DeltaWeightH2H.vector[index_3D(DeltaWeightH2H,h-1,i,j)] *=alpha;
      }
    }
  }

  if (12>11){

    CUDA_CHECK(cudaEventCreate(&start));
    CUDA_CHECK(cudaEventCreate(&stop));
    CUDA_CHECK(cudaEventRecord(start, 0));

    j_max = nupl[h];
    kernel6<<<j_max/THREADSPERBLOCK+1,THREADSPERBLOCK>>>(ranpat_gpu, DeltaWeightH2H_gpu, nupl_gpu, DeltaH2H_gpu, eta, h);
    CUDA_CHECK_KERNEL;


    copy3DToCPU(DeltaWeightH2H_gpu, DeltaWeightH2H);
    CUDA_CHECK(cudaEventRecord(stop, 0));
    CUDA_CHECK(cudaEventSynchronize(stop));
    float inc = 0;
    CUDA_CHECK(cudaEventElapsedTime(&inc, start, stop));
    printf("Time kernel 6 (ms): %f\n", inc);
  }
  else{
    ////#pragma  omp parallel for private(pointer32,p, DeltaH2H_h1_p, factor_x_indexed, factor_x)
    for( np = 0 ; np < NumPattern ; np++ ) {
      //ciclo_32f++;
      p = ranpat[np];

      j_max = nupl[h];
      //DeltaH2H_h1_p = DeltaH2H[h-1][p];


      //pointer32 = DeltaWeightH2H[h-1][0];

      ////#pragma  omp parallel for
      ////#pragma  omp simd
      for( j = 1 ; j <= j_max ; j++ ) {
        DeltaWeightH2H.vector[index_3D(DeltaWeightH2H,h-1,0,j)] += eta * DeltaH2H.vector[index_3D(DeltaH2H,h-1,p,j)];
      }
      //factor_x = ;
    }
  }
  if (13>11){
    i_max = nupl[h-1];
    j_max = nupl[h];
    CUDA_CHECK(cudaEventCreate(&start));
    CUDA_CHECK(cudaEventCreate(&stop));
    CUDA_CHECK(cudaEventRecord(start, 0));


    dim3 grid((i_max)/TILE_WIDTH+1, (j_max)/TILE_WIDTH+1, 1);
    dim3 block(TILE_WIDTH, TILE_WIDTH, 1);
    if(h==1)
    kernel7<<<grid,block>>>(ranpat_gpu, H2H_gpu, Input_gpu, DeltaWeightH2H_gpu, DeltaH2H_gpu, nupl_gpu, eta); //h==1
    else
    kernel8<<<grid,block>>>(ranpat_gpu, H2H_gpu, Input_gpu, DeltaWeightH2H_gpu, DeltaH2H_gpu, nupl_gpu, eta, h);
    CUDA_CHECK_KERNEL;


    copy3DToCPU(DeltaWeightH2H_gpu, DeltaWeightH2H);
    CUDA_CHECK(cudaEventRecord(stop, 0));
    CUDA_CHECK(cudaEventSynchronize(stop));
    float inc = 0;
    CUDA_CHECK(cudaEventElapsedTime(&inc, start, stop));
    printf("Time kernel 7+8 (ms): %f\n", inc);
  }
  else{
    for( np = 0 ; np < NumPattern ; np++ ) {
      //ciclo_32f++;
      p = ranpat[np];
      i_max = nupl[h-1];
      j_max = nupl[h];
      //#pragma  omp parallel for schedule(static) private(pointer32, i,factor_x_indexed, j)
      for( i = 1 ; i <= i_max ; i++ ) {
        //ciclo_32e++;
        //pointer32 = DeltaWeightH2H[h-1][i];

        factor_x_indexed = ((h>1)?H2H.vector[index_3D(H2H,h-2,p,i)]:Input.vector[index_2D(Input,p,i-1)]) * eta;
        ////#pragma  omp simd
        for( j = 1 ; j <= j_max ; j++ ) {
          //ciclo_32d++;
          DeltaWeightH2H.vector[index_3D(DeltaWeightH2H,h-1,i,j)] += factor_x_indexed * DeltaH2H.vector[index_3D(DeltaH2H,h-1,p,j)]; /* Matrix DeltaWeightIH = eta x (Trasposte of Input_Matrix) x DeltaH */
          /* DeltaWeight is initialized with the "momentum" */
        }
      }
    }
  }
}






#if defined(TAKETIME)
{LAP(4.);}
#endif


if(1>0){
  CUDA_CHECK(cudaEventCreate(&start));
  CUDA_CHECK(cudaEventCreate(&stop));
  CUDA_CHECK(cudaEventRecord(start, 0));


  dim3 grid((NumOutput)/TILE_WIDTH+1, (nupl[NumHL]+1)/TILE_WIDTH+1, 1);
  dim3 block(TILE_WIDTH, TILE_WIDTH, 1);
  kernel9<<<grid,block>>>(ranpat_gpu, WeightHO_gpu, DeltaWeightHO_gpu, nupl_gpu); //k [1,NumOutput] j [0,nupl[NumHL]]


  for(h=NumHL; h>0; h--) {
    dim3 grid((nupl[h])/TILE_WIDTH+1, (nupl[h-1]+1)/TILE_WIDTH+1, 1);
    kernel10<<<grid,block>>>(ranpat_gpu, WeightH2H_gpu, DeltaWeightH2H_gpu, nupl_gpu, h); //j [1,nupl[h]] i [0,nupl[h-1]]
  }
  CUDA_CHECK_KERNEL;


  copy3DToCPU(WeightH2H_gpu, WeightH2H);
  copy2DToCPU(WeightHO_gpu, WeightHO);
  CUDA_CHECK(cudaEventRecord(stop, 0));
  CUDA_CHECK(cudaEventSynchronize(stop));
  float inc = 0;
  CUDA_CHECK(cudaEventElapsedTime(&inc, start, stop));
  printf("Time kernel 9+10 (ms): %f\n", inc);
}
else{
  for( k = 1 ; k <= NumOutput ; k++ ) {    /* update weights WeightHO */
    WeightHO.vector[index_2D(WeightHO,0,k)] += DeltaWeightHO.vector[index_2D(DeltaWeightHO,0,k)] ;
    for( j = 1 ; j <= nupl[NumHL] ; j++ ) {
      WeightHO.vector[index_2D(WeightHO,j,k)] += DeltaWeightHO.vector[index_2D(DeltaWeightHO,j,k)] ;
    }
  }
  for(h=NumHL; h>0; h--) {
    for( j = 1 ; j <= nupl[h] ; j++ ) {
      WeightH2H.vector[index_3D(WeightH2H,h-1,0,j)] += DeltaWeightH2H.vector[index_3D(DeltaWeightH2H,h-1,0,j)];
      for( i = 1 ; i <= nupl[h-1] ; i++ ) {
        WeightH2H.vector[index_3D(WeightH2H,h-1,i,j)] += DeltaWeightH2H.vector[index_3D(DeltaWeightH2H,h-1,i,j)];
      }
    }
  }
}
#if defined(TAKETIME)
{LAP(5.);}
#endif
fprintf(stdout, "Epoch %-5d :   Error = %f\n", epoch, Error) ;
if(fpl) { fprintf(fpl,"Epoch %-5d :   Error = %f\n", epoch, Error);
fflush(fpl); }


copy1DToCPU((void *)Error_gpu,&Error, sizeof(Error));

if( Error < Eps ) break ;  /* stop learning when 'near enough' */


}



copy3DToCPU(WeightH2H_gpu, WeightH2H);
copy3DToCPU(DeltaWeightH2H_gpu,DeltaWeightH2H);
copy2DToCPU(DeltaWeightHO_gpu,DeltaWeightHO);
copy2DToCPU(WeightHO_gpu,WeightHO);

if(verbose) {
  printf("\nFinal Bias and Weights\n");
}
fp=Fopen(ResultFileName,"w");
fpd=Fopen(DeltaFileName,"w");
for( k = 1 ; k <= NumOutput ; k ++ ) {
  if(verbose) {
    printf("Bias H to O[%d]: %f\n",k,  WeightHO.vector[index_2D(WeightHO,0,k)]);
  }
  fprintf(fp,"%7.5f ",  WeightHO.vector[index_2D(WeightHO,0,k)]);
  fprintf(fpd,"%g ",  DeltaWeightHO.vector[index_2D(DeltaWeightHO,0,k)]);
  for( j = 1 ; j <= nupl[NumHL] ; j++ ) {
    if(verbose) {
      printf("Weight H[%d] to O[%d]: %f\n",j,k,  WeightHO.vector[index_2D(WeightHO,j,k)]);
    }
    fprintf(fp,"%7.5f ",  WeightHO.vector[index_2D(WeightHO,j,k)]);
    fprintf(fpd,"%g ",  DeltaWeightHO.vector[index_2D(DeltaWeightHO,j,k)]);
  }
  fprintf(fp,"\n");
  fprintf(fpd,"\n");
}
for(h=NumHL; h>0; h--) {
  for( j = 1 ; j <= nupl[h] ; j++ ) {
    if(verbose) {
      printf("BiasH2H[%d][%d]: %f\n",h,j,WeightH2H.vector[index_3D(WeightH2H,h-1,0,j)]);
    }
    fprintf(fp,"%7.5f ",WeightH2H.vector[index_3D(WeightH2H,h-1,0,j)]);
    fprintf(fpd,"%g ",DeltaWeightH2H.vector[index_3D(DeltaWeightH2H,h-1,0,j)]);
    for( i = 1 ; i <= nupl[h-1] ; i++ ) {
      if(verbose) {
        printf("WeightH2H[%d][%d] to H{%d]: %f\n",h,i,j,WeightH2H.vector[index_3D(WeightH2H,h-1,i,j)]);
      }
      fprintf(fp,"%7.5f ",WeightH2H.vector[index_3D(WeightH2H,h-1,i,j)]);
      fprintf(fpd,"%g ",DeltaWeightH2H.vector[index_3D(DeltaWeightH2H,h-1,i,j)]);
    }
    fprintf(fp,"\n");
    fprintf(fpd,"\n");
  }
}


#if defined(TAKETIME)
{LAP(6.);}
#endif
//CUDA_CHECK(cudaFreeHost(DeltaWeightHO_gpu.vector));
//CUDA_CHECK(cudaFreeHost(WeightHO_gpu.vector));
//CUDA_CHECK(cudaFreeHost(Input_gpu.vector));
//CUDA_CHECK(cudaFreeHost(Target_gpu.vector));
//CUDA_CHECK(cudaFreeHost(Sum_gpu.vector));
//CUDA_CHECK(cudaFreeHost(H2H_gpu.vector));
//CUDA_CHECK(cudaFreeHost(DeltaH2H_gpu.vector));
//CUDA_CHECK(cudaFreeHost(WeightH2H_gpu.vector));
//CUDA_CHECK(cudaFreeHost(Output_gpu.vector));
//CUDA_CHECK(cudaFreeHost(DeltaO_gpu.vector));
//CUDA_CHECK(cudaFreeHost(DeltaWeightH2H_gpu.vector));
//CUDA_CHECK(cudaFreeHost(ranpat_gpu));
//CUDA_CHECK(cudaFreeHost(nupl_gpu));

/*free(DeltaWeightHO_gpu.vector);
free(WeightHO.vector);
free(Input.vector);
free(Target.vector);
free(Sum.vector);
free(H2H.vector);
free(DeltaH2H.vector);
free(WeightH2H.vector);
free(Output.vector);
free(DeltaO.vector);
free(DeltaWeightH2H.vector);
free(ranpat);
free(nupl);
*/
if(fp) fclose(fp);
if(fp) fclose(fpd);
if(fpl) fclose(fpl);
#if defined(TAKETIME)
{LAP(7.);}
#endif
return 0 ;
}

/*******************************************************************************/
