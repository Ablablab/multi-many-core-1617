#include <stdio.h>

#include <cuda_runtime.h>

#define MY_CUDA_CHECK(call) {                                    \
  cudaError err = call;                                                    \
  if(cudaSuccess != err) {                                                \
    fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n",        \
    __FILE__, __LINE__, cudaGetErrorString( err) );              \
    exit(EXIT_FAILURE);                                                  \
  }}

  #define CUDA_CHECK_KERNEL { \
    cudaError_t err = cudaGetLastError(); \
    if (err != cudaSuccess)  \
    fprintf(stderr,"Error: %s\n", cudaGetErrorString(err)); \
  }

  #define DEFAULTBLOCKSIZE 1024
  #define WARPSIZE 32

  __global__ void kernel_prefix_sum(int * data, int n_elements, int * ready_counter){
    int threadId_block = threadIdx.x;
    int threadId_absolute = threadId_block + blockIdx.x*blockDim.x;
    int threadId_warp = threadId_block % WARPSIZE;
    int warpId_block = threadId_block / WARPSIZE;
    int blockId = blockIdx.x;
    __shared__ int vector_block[DEFAULTBLOCKSIZE/WARPSIZE]; //warp in block, max 32
    int d,others_int, personal_int = data[threadId_absolute];

    // sum in warp, <<= is a shift, it doubles the number
    for (d=1; d<WARPSIZE; d<<=1) {
      others_int = __shfl_up(personal_int,d); // no need for syncthread,
      // shuffle instructions are executed at the same time for all warp
      if (threadId_warp >= d)
      personal_int += others_int;
    }

    if (threadId_warp == 31) //last thread of warp store its value for other warps
    vector_block[warpId_block] = personal_int;

    __syncthreads(); //data for single warp is updated

    if (warpId_block == 0){
      int other_warp, sum_warp = vector_block[threadId_warp];

      for (d=1; d<WARPSIZE; d<<=1) {
        other_warp = __shfl_up(sum_warp,d);
        if (threadId_warp >= d) //maybe not needed
        sum_warp += other_warp;
      }

      vector_block[threadId_warp] = sum_warp;
    }

    __syncthreads();
    if(warpId_block>0)
      personal_int += vector_block[warpId_block-1];
    data[threadId_absolute] = personal_int;

    __syncthreads();

    if(blockId == 0){ // first block is ready
      if(threadId_block == 0)
      atomicAdd(ready_counter, 1);
      return;
    }

    if(blockId != 0 && threadId_block==0){
      int now = atomicAdd(ready_counter, 0);
      while(now < blockId){now = atomicAdd(ready_counter, 0);} // waiting for prev block
      //if(threadId_block == 0){
      //  printf("Da %d - Evviva il blocco precedente ha finito! mi ha svegliato %d\n", blockId, now-1);
      //}
    }
    __syncthreads();

    personal_int += data[(blockId)*blockDim.x - 1];
    data[threadId_absolute] = personal_int;

    __syncthreads();
    if(threadId_block == 0){
      atomicAdd(ready_counter, 1); // this block is ready, next one can start
    }

  }

  void ssb_prefix_sum(int *d_data, int n_elements, int * ready_counter_gpu) {
    CUDA_CHECK_KERNEL(kernel_prefix_sum<<<(n_elements-1)/DEFAULTBLOCKSIZE + 1 , DEFAULTBLOCKSIZE>>>(d_data,n_elements, ready_counter_gpu));
  }


  // This function verifies the shuffle scan result, for the simple
  // prefix sum case.
  bool CPUverify(int *h_data, int *h_result, int n_elements)
  {
    // cpu verify
    for (int i=0; i<n_elements-1; i++)
    {
      h_data[i+1] = h_data[i] + h_data[i+1];
    }

    int diff = 0;

    for (int i=0 ; i<n_elements; i++)
    {
      //	printf("%d\n",h_result[i]);
      diff += h_data[i]-h_result[i];
    }

    printf("CPU verify result diff (GPUvsCPU) = %d\n", diff);
    bool bTestResult = false;

    if (diff == 0) bTestResult = true;

    return bTestResult;
  }

  int main(int argc, char **argv) {
    int *h_data, *h_result;
    int *d_data;
    int blockSize = DEFAULTBLOCKSIZE;
    int n_elements=65536;
    int n_aligned;
    if(argc>1) {
      n_elements = atoi(argv[1]);
    }
    n_aligned=((n_elements+blockSize-1)/blockSize)*blockSize;
    int sz = sizeof(int)*n_aligned;

    printf("Starting shfl_scan\n");

    MY_CUDA_CHECK(cudaMallocHost((void **)&h_data, sizeof(int)*n_aligned));
    MY_CUDA_CHECK(cudaMallocHost((void **)&h_result, sizeof(int)*n_elements));

    //initialize data:
    printf("Computing Simple Sum test on %d (%d) elements\n",n_elements, n_aligned);
    printf("---------------------------------------------------\n");

    printf("Initialize test data\n");
    char line[1024];
    for (int i=0; i<n_elements; i++)
    {
      h_data[i] = i;
      //        fgets(line,sizeof(line),stdin);
      //        sscanf(line,"%d",&h_data[i]);
    }

    for (int i=n_elements; i<n_aligned; i++) {
      h_data[i] = 0;
    }

    printf("Scan summation for %d elements\n", n_elements);

    // initialize a timer
    cudaEvent_t start, stop;
    MY_CUDA_CHECK(cudaEventCreate(&start));
    MY_CUDA_CHECK(cudaEventCreate(&stop));
    float et = 0;
    float inc = 0;

    MY_CUDA_CHECK(cudaMalloc((void **)&d_data, sz));
    MY_CUDA_CHECK(cudaMemcpy(d_data, h_data, sz, cudaMemcpyHostToDevice));
    int * ready_counter_gpu;
    MY_CUDA_CHECK(cudaMalloc((void **)&ready_counter_gpu, sizeof(int)));
    int initial_counter = 0;
    MY_CUDA_CHECK(cudaMemcpy(ready_counter_gpu,&initial_counter , sizeof(int), cudaMemcpyHostToDevice));

    MY_CUDA_CHECK(cudaEventRecord(start, 0));
    ssb_prefix_sum(d_data,n_elements, ready_counter_gpu);
    MY_CUDA_CHECK(cudaEventRecord(stop, 0));

    MY_CUDA_CHECK(cudaEventSynchronize(stop));
    MY_CUDA_CHECK(cudaEventElapsedTime(&inc, start, stop));
    et+=inc;

    MY_CUDA_CHECK(cudaMemcpy(h_result, d_data, n_elements*sizeof(int), cudaMemcpyDeviceToHost));
    printf("Time (ms): %f\n", et);
    printf("%d elements scanned in %f ms -> %f MegaElements/s\n",
    n_elements, et, n_elements/(et/1000.0f)/1000000.0f);

    /*
    printf("data  \n");
    for (int i=0; i<n_aligned; i++) {
    printf("%d, ",h_data[i]);
  }

  printf("results  \n");
  for (int i=0; i<n_aligned; i++) {
  printf("%d, ",h_result[i]);
}
*/

bool bTestResult = CPUverify(h_data, h_result, n_elements);
MY_CUDA_CHECK(cudaFree(ready_counter_gpu));
MY_CUDA_CHECK(cudaFreeHost(h_data));
MY_CUDA_CHECK(cudaFreeHost(h_result));

return (int)bTestResult;
}
