#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <cuda.h>
#include <cuda_runtime.h>

// Macro modificata, aggiunto il do {} while(0) per avere un blocco unico di codice
#define MY_CUDA_CHECK(call)                                                     \
    do {                                                                        \
        cudaError err = call;                                                   \
        if(cudaSuccess != err) {                                                \
            fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n",       \
                    __FILE__, __LINE__, cudaGetErrorString( err) );             \
            exit(EXIT_FAILURE);                                                 \
        }                                                                       \
    } while (0);    

#define BLOCKSIZE 512
#define WARPSIZE 32


// Funzione che esegue la somma prefissa su
__device__ void prefix_sum(int *data)
{
    // vettore con le somme parziali di ogni warp
    __shared__ int warp_sums[BLOCKSIZE/WARPSIZE];

    unsigned long int id_g = threadIdx.x + blockDim.x*blockIdx.x;
    // salva data[id_g] in variabile privata per evitare race conditions
    int value = data[id_g];
    // id del thread nel warp
    unsigned int id_in_warp = threadIdx.x % WARPSIZE;
    // id del warp
    unsigned int warpId = threadIdx.x / WARPSIZE;
    // numero di warp
    unsigned int nWarps = BLOCKSIZE/WARPSIZE;

    unsigned int p;
    int n;

    // aspetta che tutti i thread abbiano caricato in value
    // il dato dalla memoria globale
    __syncthreads();

    for (p=1; p < WARPSIZE; p*=2) {
        n = __shfl_up(value,p);
        if (id_in_warp >= p) {
            value += n;
        }
    }

    // salva il risultato della riduzione parziale
    if (id_in_warp == WARPSIZE - 1) {
        warp_sums[warpId] = value;
    }
    __syncthreads();

    // Esegue prefix sum sul vettore warp_sums, per raggruppare
    // i valori temporanei calcolati da ogni warp.

    // BLOCKSIZE/WARPSIZE è al massimo 32 perché BLOCKSIZE può
    // essere al massimo 1024, e 1024/32 == 32 -> la prefix
    // sum su questo vettore la può fare un warp.
    // la facciamo fare dal warp numero 0
    if (warpId == 0) {
        // salva l'elemento del vettore in variabile privata
        // per effettuare la shuffle
        int warp_value = warp_sums[id_in_warp];

        for (p=1; p < nWarps; p*=2) {
            n = __shfl_up(warp_value,p);
            if (id_in_warp >= p) {
                warp_value += n;
            }
        }

        warp_sums[id_in_warp] = warp_value;
    }

    __syncthreads();

    // ora ogni thread dei warp con id > 0 devono aggiungere
    // warp_sums[warpId-1] a value
    if (warpId > 0) {
        value += warp_sums[warpId-1];
    }

    __syncthreads();

    data[id_g] = value;
}

__global__ void prefix_warp_sum(int *data, int *block_sums)
{
//    f o r  d = 1   t o  l o g ​2 ​ n   d o
//     for all k in parallel do
//        if k>=2^d​ then
//            x[k] = x[k – 2^(d-1)​ ] + x[k]

    // id del thread
    unsigned long int id_g = threadIdx.x + blockDim.x*blockIdx.x;
    // id del blocco
    unsigned int blockId = id_g / BLOCKSIZE;

    prefix_sum(data);

    // l'ultimo thread di ogni blocco salva il valore in un vettore
    // (serve ai blocchi successivi, come prima con i warp)
    if (threadIdx.x == BLOCKSIZE - 1) {
        block_sums[blockId] = data[id_g];
    }
}

__global__ void prefix_block_sum(int *block_sums)
{
    prefix_sum(block_sums);
}

__global__ void final_sum(int *data, int *block_sums)
{
    unsigned int id_g = threadIdx.x + blockDim.x*blockIdx.x;
    unsigned int blockId = id_g / BLOCKSIZE;

    // ogni thread di ogni blocco con id > 0 aggiunge la somma del 
    // blocco precedente al proprio elemento
    if (blockId > 0) {
        data[id_g] = data[id_g] + block_sums[blockId-1];
    }

}


void ssb_prefix_sum(int *d_data, int n_elements, int n_aligned, int blockSize, int *block_sums, int *block_sums2) {
    // nthreads è un multiplo esatto di blockSize e potenza di 2
    // gli ultimi nthreads-n_elements thread non devono fare niente,
    // però il vettore d_data ha gli ultimi nthread-n_elements elementi
    // pari a 0 quindi lavorano tutti
    unsigned int nthreads = n_aligned;
    unsigned int nblocks = n_aligned/blockSize;

    printf("Running %d blocks and %d threads\n", nblocks, nthreads);

    // Passi:
    // 1 - ogni blocco fa prefix sum e mette risultato in vettore di appoggio block_sums
    // 2 - prefix sum del vettore di appoggio block_sums
    // 3 - ogni blocco con indice > 1 somma a tutti gli elementi che gli competono il
    //     risultato di block_sums[blockId-1]
    // Per il passo 2, il kernel apposito usa 1 blocco e nblocks thread. Quindi se
    // nblocks > BLOCKSIZE il passo 2 non funziona. Per gestire questa evenienza,
    // il passo 2 viene suddiviso nei 3 passi iniziali, applicati però al vettore
    // block_sums e usando un altro vettore block_sums2 di appoggio.

    // Condizioni per cui il passo 2 viene diviso: il passo 2 viene diviso se
    // nblocks > BLOCKSIZE, ovvero n/BLOCKSIZE > BLOCKSIZE, ovvero n > BS^2.
    // Quando n = BS^2 ovvero 2^20 (se BS=1024=2^10), l'ultimo elemento
    // del vettore risultato della prefix sum contiene il numero n*(n+1)/2
    // che è circa 1.09*10^12, invece il massimo intero rappresentabile con
    // 32 bit è circa 2.14*10^9.
    // Quindi la divisione del passo 2 è inutile se si considera un vettore di interi.
    // Se si considera un vettore di long int, il massimo numero rappresentabile
    // è circa 9.2*10^18. La divisione del passo 2 non può continuare se
    // n > BS^3, ovvero con BS=2^10 viene n > 2^30 che è circa 1*10^9.
    // Con questo n, l'ultimo elemento della prefix sum è 5.7*10^17, quindi la
    // divisione del passo 2 copre quasi tutto l'intervallo di rappresentazione
    // dei long int.


    // primo kernel: effettua somma prefissa per ogni warp e dopo
    // allarga all'intero blocco. Salva i risultati di ogni blocco
    // nel vettore block_sums
    prefix_warp_sum<<<nblocks, BLOCKSIZE>>>(d_data, block_sums);

    // secondo passo: effettuare la somma prefissa del vettore block_sums, necessario
    // per il passo finale (ogni blocco infatti dovrà aggiungere la somma calcolata
    // dal blocco precedente, tranne il primo blocco)

    if (nblocks > BLOCKSIZE) {
        printf("ERROR: can't compute prefix sum (overflow)\nAborting...\n");
        exit(EXIT_FAILURE);
        prefix_warp_sum<<<nblocks/BLOCKSIZE, BLOCKSIZE>>>(block_sums, block_sums2);
        if (nblocks/BLOCKSIZE > BLOCKSIZE) {
            printf("ERROR: can't compute prefix sum\n");
            exit(EXIT_FAILURE);
        }
        prefix_block_sum<<<1, nblocks/BLOCKSIZE>>>(block_sums2);
        final_sum<<<nblocks/BLOCKSIZE, BLOCKSIZE>>>(block_sums, block_sums2);
    } else {
        prefix_block_sum<<<1, nblocks>>>(block_sums);
    }

    // passo finale: ogni blocco con indice > 1 somma a tutti gli elementi
    // la somma calcolata dal blocco precedente
    final_sum<<<nblocks, BLOCKSIZE>>>(d_data, block_sums);
}


// This function verifies the shuffle scan result, for the simple
// prefix sum case.
bool CPUverify(int *h_data, int *h_result, int n_elements)
{
    // cpu verify
    for (int i=0; i<n_elements-1; i++)
    {
        h_data[i+1] = h_data[i] + h_data[i+1];
    }

    int diff = 0;

    for (int i=0 ; i<n_elements; i++)
    {
        //printf("%d\n",h_result[i]);
        diff += h_data[i]-h_result[i];
    }

    printf("CPU verify result diff (GPUvsCPU) = %d\n", diff);
    bool bTestResult = false;

    if (diff == 0) bTestResult = true;

    return bTestResult;
}

int main(int argc, char **argv) {
    int *h_data, *h_result;
    int *d_data;
    // somme parziali dei singoli blocchi
    int *block_sums;
    int blockSize = BLOCKSIZE;
    int n_elements=65536;
    int n_elements2;
    int n_aligned;

    if(argc>1) {
    	n_elements = atoi(argv[1]);
    }

    //// finds the first power of two greater or equal to the number of elements
    //int m = 1;
    //for (m=1; ; m*=2) {
    //    if (m >= n_elements) {
    //        break;
    //    }   
    //}
    //
    //n_elements2 = m;
    // align n_elements2 so that it's a multiple of the block size
    //n_aligned=((n_elements2+blockSize-1)/blockSize)*blockSize;

    n_aligned=((n_elements+blockSize-1)/blockSize)*blockSize;
    int sz = sizeof(int)*n_aligned;

    printf("Starting shfl_scan\n");

    MY_CUDA_CHECK(cudaMallocHost((void **)&h_data, sizeof(int)*n_aligned));
    MY_CUDA_CHECK(cudaMallocHost((void **)&h_result, sizeof(int)*n_elements));

    //initialize data:
    printf("Computing Simple Sum test on %d (%d) elements\n",n_elements, n_aligned);
    printf("---------------------------------------------------\n");

    printf("Initialize test data\n");
    //char line[1024];
    for (int i=0; i<n_elements; i++)
    {
        h_data[i] = i;
//        fgets(line,sizeof(line),stdin);
//        sscanf(line,"%d",&h_data[i]);
    }

    for (int i=n_elements; i<n_aligned; i++) {
	    h_data[i] = 0;
    }

    printf("Scan summation for %d elements\n", n_elements);

    // initialize a timer
    cudaEvent_t start, stop;
    MY_CUDA_CHECK(cudaEventCreate(&start));
    MY_CUDA_CHECK(cudaEventCreate(&stop));
    float et = 0;
    float inc = 0;

    MY_CUDA_CHECK(cudaMalloc((void **)&d_data, sz));
    MY_CUDA_CHECK(cudaMemcpy(d_data, h_data, sz, cudaMemcpyHostToDevice));

    // numero di blocchi * sizeof(int)
    int nblocks = (n_aligned/blockSize);
    int block_sums_size = sizeof(int) * nblocks;
    MY_CUDA_CHECK(cudaMalloc((void **)&block_sums, block_sums_size));


    int *block_sums2 = NULL;
    // numero di elementi in block_sums / dimensione blocco
    MY_CUDA_CHECK(cudaMalloc((void **)&block_sums2, nblocks/blockSize));

    MY_CUDA_CHECK(cudaEventRecord(start, 0));

    ssb_prefix_sum(d_data,n_elements,n_aligned,blockSize,block_sums,block_sums2);

    MY_CUDA_CHECK(cudaEventRecord(stop, 0));
    MY_CUDA_CHECK(cudaEventSynchronize(stop));
    MY_CUDA_CHECK(cudaEventElapsedTime(&inc, start, stop));
    et+=inc;
    //sleep(5);
    MY_CUDA_CHECK(cudaMemcpy(h_result, d_data, n_elements*sizeof(int), cudaMemcpyDeviceToHost));
    printf("Time (ms): %f\n", et);
    printf("%d elements scanned in %f ms -> %f MegaElements/s\n",
             n_elements, et, n_elements/(et/1000.0f)/1000000.0f);

    //for (int i=0; i<n_elements; i++) {
    //    printf("%4d ", h_data[i]);
    //}
    //printf("\n");
    //for (int i=0; i<n_elements; i++) {
    //    printf("%4d ", h_result[i]);
    //}
    //printf("\n");
    printf("%d\n", h_result[n_elements-1]);

    bool bTestResult = CPUverify(h_data, h_result, n_elements);

    MY_CUDA_CHECK(cudaFreeHost(h_data));
    MY_CUDA_CHECK(cudaFreeHost(h_result));

    return (int)bTestResult;
}
